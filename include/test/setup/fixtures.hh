/*
 * Copyright (C) 2018-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FIXTURES_HH_327B2D873D064624803561323E8E3BF3
#define FIXTURES_HH_327B2D873D064624803561323E8E3BF3

#include "test/fake-src/util/cfg/handle_args.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "libfred/public_request/public_request_data.hh"
#include "libfred/public_request/public_request_type_iface.hh"
#include "libfred/registrable_object/contact/create_contact.hh"
#include "libfred/registrable_object/contact/info_contact_data.hh"
#include "libfred/registrable_object/domain/create_domain.hh"
#include "libfred/registrable_object/domain/info_domain_data.hh"
#include "libfred/registrable_object/keyset/create_keyset.hh"
#include "libfred/registrable_object/keyset/info_keyset_data.hh"
#include "libfred/registrable_object/nsset/create_nsset.hh"
#include "libfred/registrable_object/nsset/info_nsset_data.hh"
#include "libfred/registrar/create_registrar.hh"
#include "libfred/registrar/info_registrar_data.hh"
#include "libfred/zone/info_zone_data.hh"

#include <boost/optional.hpp>

#include <string>

/**
 * @file fixtures for data isolation in tests
 * for more info see documentation with examples:
 * https://admin.nic.cz/wiki/developers/fred/tests
 */

namespace Test {

// database created by fred-manager init_cz
std::string get_original_db_name();

struct create_db_template
{
    static std::string get_db_template_name();
    create_db_template();
    virtual ~create_db_template();
};

struct instantiate_db_template
{
    const std::string db_name_suffix_;/**< suffix of the name of database instance left in database cluster after fixture teardown, useful in case of more database instances per testcase */
    instantiate_db_template(const std::string& db_name_suffix = "");
    virtual ~instantiate_db_template();
private:
    std::string testcase_db_name();
};

struct HasAutoRollbackingOperationContext
{
    LibFred::OperationContextCreator ctx;
};

struct HasOperationContext
{
    ~HasOperationContext();
    LibFred::OperationContextCreator ctx;
};

struct HasRegistrar
{
    HasRegistrar(LibFred::OperationContext& ctx, std::string handle, bool is_system, bool is_internal);
    unsigned long long id;
    std::string handle;
};

struct HasZone
{
    HasZone(LibFred::OperationContext& ctx, std::string fqdn);
    unsigned long long id;
    std::string fqdn;
};

struct HasContact
{
    HasContact(LibFred::OperationContext& ctx, std::string handle, const HasRegistrar& registrar);
    unsigned long long id;
    std::string handle;
};

struct HasDomain
{
    HasDomain(
            LibFred::OperationContext& ctx,
            std::string fqdn,
            const HasRegistrar& registrar,
            const HasContact& registrant);
    unsigned long long id;
    std::string fqdn;
};

/***
 * config handlers for admin connection to db used by fixtures related to db data
 */
class HandleAdminDatabaseArgs : public HandleArgs
{
    public:
        std::string host;
        std::string port;
        std::string user;
        std::string pass;
        std::string dbname;
        std::string timeout;

        std::shared_ptr<boost::program_options::options_description> get_options_description();

        void handle( int argc, char* argv[],  FakedArgs &fa);

        std::unique_ptr<Database::StandaloneConnection> get_admin_connection();
};

struct Registrar
{
    explicit Registrar(LibFred::OperationContext& ctx, LibFred::CreateRegistrar create);
    LibFred::InfoRegistrarData data;
};

struct SystemRegistrar
{
    explicit SystemRegistrar(LibFred::OperationContext& ctx, LibFred::CreateRegistrar create);
    LibFred::InfoRegistrarData data;
};

struct Zone
{
    explicit Zone(
            LibFred::OperationContext& ctx,
            const char* zone,
            bool idn_enabled = false);
    LibFred::Zone::NonEnumZone data;
};

struct EnumZone
{
    explicit EnumZone(
            LibFred::OperationContext& ctx,
            const char* zone,
            int enum_validation_period_in_months);
    LibFred::Zone::EnumZone data;
};

struct CzZone : Zone
{
    explicit CzZone(LibFred::OperationContext& ctx);
    static const char* fqdn() noexcept;
    static std::string fqdn(const char* subdomain);
    static std::string fqdn(const std::string& subdomain);
};

struct CzEnumZone : EnumZone
{
    explicit CzEnumZone(LibFred::OperationContext& ctx);
    static const char* fqdn() noexcept;
    static std::string fqdn(unsigned long long subdomain);
};

struct InitDomainNameCheckers
{
    explicit InitDomainNameCheckers(LibFred::OperationContext& ctx);
};

struct Contact
{
    explicit Contact(const LibFred::OperationContext& ctx, LibFred::CreateContact create);
    LibFred::InfoContactData data;
};

struct Domain
{
    explicit Domain(const LibFred::OperationContext& ctx, LibFred::CreateDomain create);
    LibFred::InfoDomainData data;
};

struct Keyset
{
    explicit Keyset(const LibFred::OperationContext& ctx, LibFred::CreateKeyset create);
    LibFred::InfoKeysetData data;
};

struct Nsset
{
    explicit Nsset(const LibFred::OperationContext& ctx, LibFred::CreateNsset create);
    LibFred::InfoNssetData data;
};

struct PublicRequest
{
    explicit PublicRequest(
            const LibFred::OperationContext& ctx,
            const std::string& type,
            unsigned long long object_id,
            const boost::optional<Test::Registrar>& registrar,
            const std::string& email_to_answer,
            LibFred::PublicRequest::Status::Enum status,
            LibFred::PublicRequest::OnStatusAction::Enum on_status_action,
            std::chrono::seconds create_time_shift,
            const boost::optional<unsigned long long>& create_request_id,
            const boost::optional<std::chrono::seconds>& resolve_time_shift,
            const boost::optional<unsigned long long>& resolve_request_id);
    LibFred::PublicRequest::PublicRequestData data;
};

namespace Setter {

LibFred::CreateRegistrar registrar(LibFred::CreateRegistrar create, int index = 0);
LibFred::CreateRegistrar system_registrar(LibFred::CreateRegistrar create, int index = 0);
LibFred::CreateContact contact(LibFred::CreateContact create, int index = 0);
LibFred::CreateDomain domain(LibFred::CreateDomain create, int index = 0);
LibFred::CreateKeyset keyset(LibFred::CreateKeyset create, int index = 0);
LibFred::CreateNsset nsset(LibFred::CreateNsset create, int index = 0);

}//namespace Test::Setter

}//namespace Test

#endif//FIXTURES_HH_327B2D873D064624803561323E8E3BF3
