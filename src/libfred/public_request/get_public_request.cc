/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/public_request/get_public_request.hh"

#include <boost/uuid/string_generator.hpp>

#include <algorithm>
#include <exception>
#include <utility>

namespace LibFred {
namespace PublicRequest {

namespace {

template <typename T>
constexpr Filter<T> do_not_filter()
{
    return Filter<T>{{}};
}

struct ByPublicRequest : boost::static_visitor<void>
{
    explicit ByPublicRequest(std::string& sql, Database::QueryParams& params)
        : sql{sql},
          params{params}
    { }
    void operator()(unsigned long long id) const
    {
        params.push_back(id);
        sql += "pr.id = $" + std::to_string(params.size()) + "::BIGINT";
    }
    void operator()(const boost::uuids::uuid& uuid) const
    {
        params.push_back(uuid);
        sql += "pr.uuid = $" + std::to_string(params.size()) + "::UUID";
    }
    std::string& sql;
    Database::QueryParams& params;
};

template <Object_Type::Enum object_type>
std::string make_sql_name_comparison(std::size_t param_number)
{
    return "UPPER(obr.name) = UPPER($" + std::to_string(param_number) + "::TEXT)";
}

template <>
std::string make_sql_name_comparison<Object_Type::domain>(std::size_t param_number)
{
    return "obr.name = LOWER($" + std::to_string(param_number) + "::TEXT)";
}

template <Object_Type::Enum object_type>
struct ByObjectRef : boost::static_visitor<void>
{
    explicit ByObjectRef(std::string& sql, Database::QueryParams& params)
        : sql{sql},
          params{params}
    { }
    void operator()(unsigned long long id) const
    {
        params.push_back(id);
        sql += "obr.id = $" + std::to_string(params.size()) + "::BIGINT AND "
               "obr.type = get_object_type_id('" + Conversion::Enums::to_db_handle(object_type) + "')";
    }
    void operator()(const boost::uuids::uuid& uuid) const
    {
        params.push_back(uuid);
        sql += "obr.uuid = $" + std::to_string(params.size()) + "::UUID AND "
               "obr.type = get_object_type_id('" + Conversion::Enums::to_db_handle(object_type) + "')";
    }
    void operator()(const std::string& handle) const
    {
        params.push_back(handle);
        sql += make_sql_name_comparison<object_type>(params.size()) + " AND "
               "obr.type = get_object_type_id('" + Conversion::Enums::to_db_handle(object_type) + "') AND "
               "obr.erdate IS NULL";
    }
    std::string& sql;
    Database::QueryParams& params;
};

PublicRequestData::ObjectDataLight make_object_data_light(
        object_type type,
        unsigned long long id,
        const boost::uuids::uuid& uuid,
        std::string handle)
{
    switch (type)
    {
        case contact: return PublicRequestData::ContactDataLight{id, uuid, std::move(handle)};
        case domain: return PublicRequestData::DomainDataLight{id, uuid, std::move(handle)};
        case keyset: return PublicRequestData::KeysetDataLight{id, uuid, std::move(handle)};
        case nsset: return PublicRequestData::NssetDataLight{id, uuid, std::move(handle)};
    }
    struct UnknownType : std::exception
    {
        const char* what() const noexcept override { return "unexpected object type"; }
    };
    throw UnknownType{};
}

std::string get_empty_string_if_null(const Database::Value& value)
{
    return value.isnull() ? std::string{}
                          : static_cast<std::string>(value);
}

PublicRequestData make_public_request_data(
        const Database::Value& id,
        const Database::Value& uuid,
        const Database::Value& object_type,
        const Database::Value& object_id,
        const Database::Value& object_uuid,
        const Database::Value& object_handle,
        const Database::Value& type,
        const Database::Value& registrar_id,
        const Database::Value& registrar_uuid,
        const Database::Value& registrar_handle,
        const Database::Value& create_time,
        const Database::Value& create_request_id,
        const Database::Value& resolve_time,
        const Database::Value& resolve_request_id,
        const Database::Value& status,
        const Database::Value& on_status_action,
        const Database::Value& email_to_answer)
{
    return PublicRequestData{
            static_cast<unsigned long long>(id),
            boost::uuids::string_generator{}(static_cast<std::string>(uuid)),
            make_object_data_light(
                    object_type_from_db_handle(static_cast<std::string>(object_type)),
                    static_cast<unsigned long long>(object_id),
                    boost::uuids::string_generator{}(static_cast<std::string>(object_uuid)),
                    static_cast<std::string>(object_handle)),
            static_cast<std::string>(type),
            registrar_id.isnull() ? boost::none
                                  : boost::make_optional(PublicRequestData::RegistrarDataLight{
                                        static_cast<unsigned long long>(registrar_id),
                                        boost::uuids::string_generator{}(static_cast<std::string>(registrar_uuid)),
                                        static_cast<std::string>(registrar_handle)}),
            static_cast<PublicRequestData::TimePoint>(create_time),
            get_empty_string_if_null(create_request_id),
            resolve_time.isnull() ? boost::none
                                 : boost::make_optional(static_cast<PublicRequestData::TimePoint>(resolve_time)),
            get_empty_string_if_null(resolve_request_id),
            Conversion::Enums::from_db_handle<Status>(static_cast<std::string>(status)),
            Conversion::Enums::from_db_handle<OnStatusAction>(static_cast<std::string>(on_status_action)),
            get_empty_string_if_null(email_to_answer)};
}

void apply_filter(
        std::string& sql,
        Database::QueryParams& params,
        const Filter<Status::Enum>& filter)
{
    if (!filter.any_of.empty())
    {
        std::string in_set;
        std::for_each(begin(filter.any_of), end(filter.any_of), [&](const auto status)
        {
            params.push_back(Conversion::Enums::to_db_handle(status));
            in_set += (in_set.empty() ? "$" + std::to_string(params.size()) + "::TEXT"
                                      : ", $" + std::to_string(params.size()) + "::TEXT");
        });
        sql += " AND eprs.name IN (" + in_set + ")";
    }
}

void apply_filter(
        std::string& sql,
        Database::QueryParams& params,
        const Filter<OnStatusAction::Enum>& filter)
{
    if (!filter.any_of.empty())
    {
        std::string in_set;
        std::for_each(begin(filter.any_of), end(filter.any_of), [&](const auto action)
        {
            params.push_back(Conversion::Enums::to_db_handle(action));
            in_set += (in_set.empty() ? "$" + std::to_string(params.size()) + "::enum_on_status_action_type"
                                      : ", $" + std::to_string(params.size()) + "::enum_on_status_action_type");
        });
        sql += " AND pr.on_status_action IN (" + in_set + ")";
    }
}

void apply_filter(
        std::string& sql,
        Database::QueryParams& params,
        const Filter<std::string>& filter)
{
    if (!filter.any_of.empty())
    {
        std::string in_set;
        std::for_each(begin(filter.any_of), end(filter.any_of), [&](auto&& type)
        {
            params.push_back(type);
            in_set += (in_set.empty() ? "$" + std::to_string(params.size()) + "::TEXT"
                                      : ", $" + std::to_string(params.size()) + "::TEXT");
        });
        sql += " AND eprt.name IN (" + in_set + ")";
    }
}

void apply_ordering(std::string& sql, ChronologicOrder ordering)
{
    switch (ordering)
    {
        case ChronologicOrder::from_newest:
            sql += " "
                    "ORDER BY pr.create_time DESC, "
                             "pr.id DESC";
            return;
        case ChronologicOrder::from_oldest:
            sql += " "
                    "ORDER BY pr.create_time, "
                             "pr.id";
            return;
    }
    struct UnknownOrder : std::exception
    {
        const char* what() const noexcept override { return "unexpected ordering specification"; }
    };
    throw UnknownOrder{};
}

}//namespace LibFred::PublicRequest::{anonymous}

GetPublicRequest::GetPublicRequest(unsigned long long id)
    : public_request_ref_{id}
{ }

GetPublicRequest::GetPublicRequest(const boost::uuids::uuid& uuid)
    : public_request_ref_{uuid}
{ }

PublicRequestData GetPublicRequest::exec(const OperationContext& ctx) const
{
    std::string sql =
            "SELECT pr.id, "
                   "pr.uuid, "
                   "eot.name, "
                   "obr.id, "
                   "obr.uuid, "
                   "obr.name, "
                   "eprt.name, "
                   "r.id, "
                   "r.uuid, "
                   "r.handle, "
                   "pr.create_time, "
                   "pr.create_request_id, "
                   "pr.resolve_time, "
                   "pr.resolve_request_id, "
                   "eprs.name, "
                   "pr.on_status_action, "
                   "pr.email_to_answer "
              "FROM public_request pr "
              "JOIN enum_public_request_type eprt ON eprt.id = pr.request_type "
              "JOIN enum_public_request_status eprs ON eprs.id = pr.status "
         "LEFT JOIN registrar r ON r.id = pr.registrar_id "
              "JOIN public_request_objects_map prom ON prom.request_id = pr.id "
              "JOIN object_registry obr ON obr.id = prom.object_id "
              "JOIN enum_object_type eot ON eot.id = obr.type "
             "WHERE ";
    Database::QueryParams params;
    boost::apply_visitor(ByPublicRequest{sql, params}, public_request_ref_);
    const auto dbres = ctx.get_conn().exec_params(sql, params);
    if (dbres.size() <= 0)
    {
        struct Exception : PublicRequestDoesNotExist
        {
            const char* what() const noexcept override { return "public request does not exist"; }
        };
        throw Exception{};
    }
    if (1 < dbres.size())
    {
        struct UnexpectedResult : std::exception
        {
            const char* what() const noexcept override { return "too many public requests or too many objects coupled to it"; }
        };
        throw UnexpectedResult{};
    }
    const auto columns = dbres[0];
    return make_public_request_data(
            columns[0],
            columns[1],
            columns[2],
            columns[3],
            columns[4],
            columns[5],
            columns[6],
            columns[7],
            columns[8],
            columns[9],
            columns[10],
            columns[11],
            columns[12],
            columns[13],
            columns[14],
            columns[15],
            columns[16]);
}

template <Object_Type::Enum object_type>
GetPublicRequestsOf<object_type>::GetPublicRequestsOf(
        unsigned long long object_id,
        Filter<Status::Enum> status,
        Filter<OnStatusAction::Enum> on_status_action,
        Filter<std::string> type,
        ChronologicOrder order)
    : object_ref_{object_id},
      status_{std::move(status)},
      on_status_action_{std::move(on_status_action)},
      type_{std::move(type)},
      order_{order}
{ }

template <Object_Type::Enum object_type>
GetPublicRequestsOf<object_type>::GetPublicRequestsOf(
        const boost::uuids::uuid& object_uuid,
        Filter<Status::Enum> status,
        Filter<OnStatusAction::Enum> on_status_action,
        Filter<std::string> type,
        ChronologicOrder order)
    : object_ref_{object_uuid},
      status_{std::move(status)},
      on_status_action_{std::move(on_status_action)},
      type_{std::move(type)},
      order_{order}
{ }

template <Object_Type::Enum object_type>
GetPublicRequestsOf<object_type>::GetPublicRequestsOf(
        std::string object_handle,
        Filter<Status::Enum> status,
        Filter<OnStatusAction::Enum> on_status_action,
        Filter<std::string> type,
        ChronologicOrder order)
    : object_ref_{std::move(object_handle)},
      status_{std::move(status)},
      on_status_action_{std::move(on_status_action)},
      type_{std::move(type)},
      order_{order}
{ }

template <Object_Type::Enum object_type>
auto GetPublicRequestsOf<object_type>::filter_by(Filter<Status::Enum> status) -> GetPublicRequestsOf&
{
    status_ = std::move(status);
    return *this;
}

template <Object_Type::Enum object_type>
auto GetPublicRequestsOf<object_type>::filter_by(Filter<OnStatusAction::Enum> on_status_action) -> GetPublicRequestsOf&
{
    on_status_action_ = std::move(on_status_action);
    return *this;
}

template <Object_Type::Enum object_type>
auto GetPublicRequestsOf<object_type>::filter_by(Filter<std::string> type) -> GetPublicRequestsOf&
{
    type_ = std::move(type);
    return *this;
}

template <Object_Type::Enum object_type>
auto GetPublicRequestsOf<object_type>::set_order(ChronologicOrder order) -> GetPublicRequestsOf&
{
    order_ = order;
    return *this;
}

template <Object_Type::Enum object_type>
std::vector<PublicRequestData> GetPublicRequestsOf<object_type>::exec(const OperationContext& ctx) const
{
    std::string sql =
            "SELECT pr.id, "
                   "pr.uuid, "
                   "eot.name, "
                   "obr.id, "
                   "obr.uuid, "
                   "obr.name, "
                   "eprt.name, "
                   "r.id, "
                   "r.uuid, "
                   "r.handle, "
                   "pr.create_time, "
                   "pr.create_request_id, "
                   "pr.resolve_time, "
                   "pr.resolve_request_id, "
                   "eprs.name, "
                   "pr.on_status_action, "
                   "pr.email_to_answer "
              "FROM public_request pr "
              "JOIN enum_public_request_type eprt ON eprt.id = pr.request_type "
              "JOIN enum_public_request_status eprs ON eprs.id = pr.status "
         "LEFT JOIN registrar r ON r.id = pr.registrar_id "
              "JOIN public_request_objects_map prom ON prom.request_id = pr.id "
              "JOIN object_registry obr ON obr.id = prom.object_id "
              "JOIN enum_object_type eot ON eot.id = obr.type "
             "WHERE ";
    Database::QueryParams params;
    boost::apply_visitor(ByObjectRef<object_type>{sql, params}, object_ref_);
    apply_filter(sql, params, status_);
    apply_filter(sql, params, on_status_action_);
    apply_filter(sql, params, type_);
    apply_ordering(sql, order_);
    const auto dbres = ctx.get_conn().exec_params(sql, params);
    std::vector<PublicRequestData> result;
    for (auto idx = 0ull; idx < dbres.size(); ++idx)
    {
        const auto columns = dbres[idx];
        result.push_back(make_public_request_data(
                columns[0],
                columns[1],
                columns[2],
                columns[3],
                columns[4],
                columns[5],
                columns[6],
                columns[7],
                columns[8],
                columns[9],
                columns[10],
                columns[11],
                columns[12],
                columns[13],
                columns[14],
                columns[15],
                columns[16]));
    }
    return result;
}

template class GetPublicRequestsOf<Object_Type::contact>;
template class GetPublicRequestsOf<Object_Type::domain>;
template class GetPublicRequestsOf<Object_Type::keyset>;
template class GetPublicRequestsOf<Object_Type::nsset>;

}//namespace LibFred::PublicRequest
}//namespace LibFred

using namespace LibFred::PublicRequest;

Filter<Status::Enum> LibFred::PublicRequest::any_status()
{
    return do_not_filter<Status::Enum>();
}

Filter<OnStatusAction::Enum> LibFred::PublicRequest::any_on_status_action()
{
    return do_not_filter<OnStatusAction::Enum>();
}

Filter<std::string> LibFred::PublicRequest::any_type()
{
    return do_not_filter<std::string>();
}
