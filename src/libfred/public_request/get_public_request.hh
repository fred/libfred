/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_PUBLIC_REQUEST_HH_A7650B758BB0D16D861A088E2EFC05B7//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_PUBLIC_REQUEST_HH_A7650B758BB0D16D861A088E2EFC05B7

#include "libfred/object/registry_object_type.hh"
#include "libfred/opcontext.hh"
#include "libfred/public_request/public_request_data.hh"

#include <boost/variant.hpp>

#include <exception>
#include <set>
#include <string>
#include <vector>

namespace LibFred {
namespace PublicRequest {

struct PublicRequestDoesNotExist : std::exception { };

class GetPublicRequest
{
public:
    explicit GetPublicRequest(unsigned long long id);
    explicit GetPublicRequest(const boost::uuids::uuid& uuid);

    PublicRequestData exec(const OperationContext& ctx) const;
private:
    using PublicRequestRef = boost::variant<unsigned long long, boost::uuids::uuid>;
    PublicRequestRef public_request_ref_;
};

template <typename T>
struct Filter
{
    std::set<T> any_of; // empty means "do not filter"
};

Filter<Status::Enum> any_status();
Filter<OnStatusAction::Enum> any_on_status_action();
Filter<std::string> any_type();

enum class ChronologicOrder
{
    from_newest,
    from_oldest
};

template <Object_Type::Enum object_type>
class GetPublicRequestsOf
{
public:
    explicit GetPublicRequestsOf(
            unsigned long long object_id,
            Filter<Status::Enum> status = any_status(),
            Filter<OnStatusAction::Enum> on_status_action = any_on_status_action(),
            Filter<std::string> type = any_type(),
            ChronologicOrder order = ChronologicOrder::from_newest);
    explicit GetPublicRequestsOf(
            const boost::uuids::uuid& object_uuid,
            Filter<Status::Enum> status = any_status(),
            Filter<OnStatusAction::Enum> on_status_action = any_on_status_action(),
            Filter<std::string> type = any_type(),
            ChronologicOrder order = ChronologicOrder::from_newest);
    explicit GetPublicRequestsOf(
            std::string, // object_handle or object_fqdn
            Filter<Status::Enum> status = any_status(),
            Filter<OnStatusAction::Enum> on_status_action = any_on_status_action(),
            Filter<std::string> type = any_type(),
            ChronologicOrder order = ChronologicOrder::from_newest);

    GetPublicRequestsOf& filter_by(Filter<Status::Enum> status = any_status());
    GetPublicRequestsOf& filter_by(Filter<OnStatusAction::Enum> on_status_action = any_on_status_action());
    GetPublicRequestsOf& filter_by(Filter<std::string> type = any_type());
    GetPublicRequestsOf& set_order(ChronologicOrder order = ChronologicOrder::from_newest);
    
    std::vector<PublicRequestData> exec(const OperationContext& ctx) const;
private:
    using ObjectRef = boost::variant<unsigned long long, boost::uuids::uuid, std::string>;
    ObjectRef object_ref_;
    Filter<Status::Enum> status_;
    Filter<OnStatusAction::Enum> on_status_action_;
    Filter<std::string> type_;
    ChronologicOrder order_;
};

using GetPublicRequestsOfContact = GetPublicRequestsOf<Object_Type::contact>;
using GetPublicRequestsOfDomain = GetPublicRequestsOf<Object_Type::domain>;
using GetPublicRequestsOfKeyset = GetPublicRequestsOf<Object_Type::keyset>;
using GetPublicRequestsOfNsset = GetPublicRequestsOf<Object_Type::nsset>;

}//namespace LibFred::PublicRequest
}//namespace LibFred

#endif//GET_PUBLIC_REQUEST_HH_A7650B758BB0D16D861A088E2EFC05B7
