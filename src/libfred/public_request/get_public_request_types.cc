/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libfred/public_request/get_public_request_types.hh"

#include "src/util/log/log.hh"

#include <exception>

namespace LibFred {
namespace PublicRequest {

PublicRequestTypes GetPublicRequestTypes::exec(const OperationContext& ctx)
{
    auto types = PublicRequestTypes{};
    const auto dbres = ctx.get_conn().exec("SELECT name FROM enum_public_request_type");
    for (auto idx = 0ul; idx < dbres.size(); ++idx)
    {
        const auto insertion_result = types.insert(static_cast<std::string>(dbres[idx][0]));
        if (!insertion_result.second)
        {
            FREDLOG_INFO("type " + (*insertion_result.first) + " is not unique");
            struct UniquenessViolation : std::exception
            {
                const char* what() const noexcept override { return "type has to be unique"; }
            };
            throw UniquenessViolation{};
        }
    }
    return types;
}

}//namespace LibFred::PublicRequest
}//namespace LibFred
