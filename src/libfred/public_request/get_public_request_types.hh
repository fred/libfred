/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_PUBLIC_REQUEST_TYPES_HH_1BE0F38889FDEA0A2CFEEA8BFE371D6C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_PUBLIC_REQUEST_TYPES_HH_1BE0F38889FDEA0A2CFEEA8BFE371D6C

#include "libfred/opcontext.hh"

#include <set>
#include <string>

namespace LibFred {
namespace PublicRequest {

using PublicRequestTypes = std::set<std::string>;

class GetPublicRequestTypes
{
public:
    static PublicRequestTypes exec(const OperationContext& ctx);
};

}//namespace LibFred::PublicRequest
}//namespace LibFred

#endif//GET_PUBLIC_REQUEST_TYPES_HH_1BE0F38889FDEA0A2CFEEA8BFE371D6C
