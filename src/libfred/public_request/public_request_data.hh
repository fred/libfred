/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PUBLIC_REQUEST_DATA_HH_FE5191302E1CBCD2F26585580835404C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PUBLIC_REQUEST_DATA_HH_FE5191302E1CBCD2F26585580835404C

#include "libfred/public_request/public_request_on_status_action.hh"
#include "libfred/public_request/public_request_status.hh"

#include <boost/optional.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <map>
#include <string>

namespace LibFred {
namespace PublicRequest {

/**
 * Information about public request.
 */
struct PublicRequestData
{
    typedef std::chrono::system_clock::time_point TimePoint;
    struct ContactDataLight
    {
        unsigned long long id;
        boost::uuids::uuid uuid;
        std::string handle;
    };
    struct DomainDataLight
    {
        unsigned long long id;
        boost::uuids::uuid uuid;
        std::string fqdn;
    };
    struct KeysetDataLight
    {
        unsigned long long id;
        boost::uuids::uuid uuid;
        std::string handle;
    };
    struct NssetDataLight
    {
        unsigned long long id;
        boost::uuids::uuid uuid;
        std::string handle;
    };
    struct RegistrarDataLight
    {
        unsigned long long id;
        boost::uuids::uuid uuid;
        std::string handle;
    };
    using ObjectDataLight = boost::variant<ContactDataLight, DomainDataLight, KeysetDataLight, NssetDataLight>;
    unsigned long long id;
    boost::uuids::uuid uuid;
    ObjectDataLight object_data;
    std::string type;
    boost::optional<RegistrarDataLight> registrar_data;
    TimePoint create_time;
    std::string create_request_id;
    boost::optional<TimePoint> resolve_time;
    std::string resolve_request_id;
    Status::Enum status;
    OnStatusAction::Enum on_status_action;
    std::string email_to_answer;
};

}//namespace LibFred::PublicRequest
}//namespace LibFred

#endif//PUBLIC_REQUEST_DATA_HH_FE5191302E1CBCD2F26585580835404C
