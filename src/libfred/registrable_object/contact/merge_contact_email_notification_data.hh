/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MERGE_CONTACT_EMAIL_NOTIFICATION_DATA_HH_4CD8CF4F78674DAAA4FEE4D2A05BF30D
#define MERGE_CONTACT_EMAIL_NOTIFICATION_DATA_HH_4CD8CF4F78674DAAA4FEE4D2A05BF30D

#include "libfred/opexception.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrable_object/contact/merge_contact.hh"

#include <boost/uuid/uuid_io.hpp>

#include <string>
#include <vector>

namespace LibFred {

struct MergeContactEmailNotificationInput
{
    MergeContactEmailNotificationInput() {}
    MergeContactEmailNotificationInput(
            const std::string& _src_contact_handle,
            const std::string& _dst_contact_handle,
            const MergeContactOutput& _merge_output)
        : src_contact_handle(_src_contact_handle),
          dst_contact_handle(_dst_contact_handle),
          merge_output(_merge_output)
    {}

    std::string src_contact_handle;//source contact identifier
    std::string dst_contact_handle;//destination contact identifier
    MergeContactOutput merge_output;//result of merge operation
};

struct MergeContactNotificationEmail
{
    std::string dst_contact_handle;
    std::string dst_contact_roid;
    std::vector<std::string> domain_registrant_list;
    std::vector<std::string> domain_admin_list;
    std::vector<std::string> nsset_tech_list;
    std::vector<std::string> keyset_tech_list;
    std::vector<std::string> removed_list;
    std::vector<std::string> removed_roid_list;
    boost::uuids::uuid dst_contact_uuid;
    std::vector<boost::uuids::uuid> updated_domains;
    std::vector<boost::uuids::uuid> updated_nssets;
    std::vector<boost::uuids::uuid> updated_keysets;
    std::vector<boost::uuids::uuid> removed_contacts;

    bool operator==(const MergeContactNotificationEmail& _other) const;
};

struct SortedContactNotificationEmail
{
    std::string dst_contact_handle;
    std::set<std::string> domain_registrant_list;
    std::set<std::string> domain_admin_list;
    std::set<std::string> nsset_tech_list;
    std::set<std::string> keyset_tech_list;
    std::set<std::string> removed_list;
    std::set<std::string> removed_roid_list;
    boost::uuids::uuid dst_contact_uuid;
    std::set<boost::uuids::uuid> updated_domains;
    std::set<boost::uuids::uuid> updated_nssets;
    std::set<boost::uuids::uuid> updated_keysets;
    std::set<boost::uuids::uuid> removed_contacts;
};

class MergeContactEmailNotificationData
{
public:
    DECLARE_EXCEPTION_DATA(invalid_contact_handle, std::string);
    DECLARE_EXCEPTION_DATA(invalid_registry_object_identifier, std::string);
    struct Exception : virtual LibFred::OperationException,
                               ExceptionData_invalid_contact_handle<Exception>,
                               ExceptionData_invalid_registry_object_identifier<Exception>
    {};

    explicit MergeContactEmailNotificationData(const std::vector<MergeContactEmailNotificationInput>&);
    std::vector<MergeContactNotificationEmail> exec(const OperationContext& ctx);
private:
    void update_email(
            std::vector<MergeContactEmailNotificationInput>::iterator i,
            SortedContactNotificationEmail& email);
    std::vector<MergeContactEmailNotificationInput> merge_contact_data_;
};

struct MergeContactNotificationEmailWithAddr
{
    std::string notification_email_addr;
    MergeContactNotificationEmail email_data;
};

class MergeContactNotificationEmailAddr
{
public:
    DECLARE_EXCEPTION_DATA(invalid_registry_object_identifier, std::string);
    struct Exception : virtual LibFred::OperationException,
                               ExceptionData_invalid_registry_object_identifier<Exception>
    {};

    explicit MergeContactNotificationEmailAddr(const std::vector<MergeContactNotificationEmail>&);
    std::vector<MergeContactNotificationEmailWithAddr> exec(const OperationContext& ctx);
private:
    const std::vector<MergeContactNotificationEmail> email_data_;
};

}//namespace LibFred

#endif//MERGE_CONTACT_EMAIL_NOTIFICATION_DATA_HH_4CD8CF4F78674DAAA4FEE4D2A05BF30D
