/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUCTION_ID_HH_02501A96BAE61650482B21BF6CA7326E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define AUCTION_ID_HH_02501A96BAE61650482B21BF6CA7326E

#include <libstrong/type.hh>

#include <boost/variant.hpp>
#include <boost/uuid/uuid.hpp>

#include <cstdint>
#include <string>

namespace LibFred {
namespace Domain {
namespace Auction {

using AuctionId = LibStrong::ArithmeticSequence<std::uint64_t, struct AuctionIdTag_>;
using AuctionUuid = LibStrong::ArithmeticSequence<boost::uuids::uuid, struct AuctionUuidTag_>;
using ExternalAuctionId = LibStrong::Type<std::string, struct ExternalAuctionIdTag_>;
using Fqdn = LibStrong::Type<std::string, struct FqdnTag_>;
using WinnerId = LibStrong::Type<boost::variant<std::uint64_t, boost::uuids::uuid>, struct WinnerIdTag_>;

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

#endif//AUCTION_ID_HH_02501A96BAE61650482B21BF6CA7326E
