/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/registrable_object/domain/auction/create_auction.hh"
#include "src/libfred/registrable_object/domain/check_domain.hh"
#include "src/libfred/zone/info_zone.hh"
#include "src/libfred/zone/zone.hh"

#include "src/util/types/convert_sql_std_chrono_types.hh"

#include "liblog/liblog.hh"

#include <utility>

namespace LibFred {
namespace Domain {
namespace Auction {

namespace {

template <typename Error, typename MsgGen>
[[noreturn]] void raise(MsgGen msg_gen)
{
    struct Exception : Error
    {
        Exception(MsgGen msg_gen) : msg_gen{std::move(msg_gen)} {}
        const char* what() const noexcept override { return msg_gen(); }
        MsgGen msg_gen;
    };
    throw Exception{std::move(msg_gen)};
}

struct GetAuctionEnabled : boost::static_visitor<bool>
{
    bool operator()(const boost::blank) const noexcept
    {
        return false;
    }
    template <typename ZoneData>
    bool operator()(const ZoneData& data) const noexcept
    {
        return data.auction_enabled;
    }
};

Database::QueryParam to_query_param(const std::chrono::system_clock::time_point& t)
{
    return Database::QueryParam{SqlConvert<std::chrono::system_clock::time_point>::convert_from_time_point(t)};
}

}//namespace LibFred::Domain::Auction::{anonymous}

CreateAuction::CreateAuction(std::string domain_fqdn)
    : domain_fqdn_{std::move(domain_fqdn)},
      start_at_{boost::none}
{ }

CreateAuction::CreateAuction(
        std::string domain_fqdn,
        const std::chrono::system_clock::time_point& start_at)
    : domain_fqdn_{std::move(domain_fqdn)},
      start_at_{start_at}
{ }

CreateAuction::Result CreateAuction::exec(const OperationContext& ctx) const
{
    try
    {
        const CheckDomain check_domain{domain_fqdn_};
        if (check_domain.is_invalid_syntax(ctx))
        {
            raise<InvalidFqdnSyntax>([]() { return "fqdn invalid"; });
        }
        if (check_domain.will_be_blacklisted_after(ctx, start_at_))
        {
            raise<FqdnBlacklisted>([]() { return "fqdn blacklisted"; });
        }
        if (check_domain.is_registered(ctx))
        {
            raise<DomainRegistered>([]() { return "domain is registered"; });
        }
        if (check_domain.is_auctioned(ctx))
        {
            raise<FqdnAlreadyInAuction>([]() { return "fqdn already in auction"; });
        }
        const auto auction_enabled = [this](const OperationContext& ctx)
        {
            try
            {
                const auto zone = Zone::find_zone_in_fqdn(ctx, domain_fqdn_);
                const auto zone_data = Zone::InfoZone{zone.name}.exec(ctx);
                return boost::apply_visitor(GetAuctionEnabled{}, zone_data);
            }
            catch (...) { }
            return false;
        }(ctx);
        if (!auction_enabled)
        {
            raise<AuctionDisabled>([]() { return "auction not enabled"; });
        }
        const auto dbres = ctx.get_conn().exec_params(
                "INSERT INTO domain_auction (fqdn, created_at, external_id) "
                       "VALUES($1::TEXT, GREATEST($2::TIMESTAMP WITHOUT TIME ZONE, NOW()), NULL) "
                       "RETURNING id",
                {Database::QueryParam{domain_fqdn_},
                 start_at_ == boost::none ? Database::NullQueryParam
                                          : to_query_param(*start_at_)});
        if (dbres.size() != 1)
        {
            raise<Exception>([]() { return "exactly one row expected"; });
        }
        return AuctionId{static_cast<std::uint64_t>(dbres[0][0])};
    }
    catch (const Exception& e)
    {
        LIBLOG_INFO("CreateAuction::Exception caught: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
}

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred
