/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_AUCTION_HH_8E3AB16B490FC3EF7157549FF287CEDE//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CREATE_AUCTION_HH_8E3AB16B490FC3EF7157549FF287CEDE

#include "libfred/registrable_object/domain/auction/auction_id.hh"

#include "libfred/opcontext.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <exception>
#include <string>

namespace LibFred {
namespace Domain {
namespace Auction {

/**
 * Add domain into auction.
 */
class CreateAuction
{
public:
    struct Exception : std::exception {};
    struct DomainRegistered : Exception {};/**< only non-registered (deleted) domain can be added into auction */
    struct FqdnAlreadyInAuction : Exception {};
    struct AuctionDisabled : Exception {};
    struct InvalidFqdnSyntax : Exception {};
    struct FqdnBlacklisted : Exception {};

    /**
    * Constructor with mandatory parameter.
    * @param domain_fqdn fqdn of domain added into auction
    */
    explicit CreateAuction(std::string domain_fqdn);

    /**
    * Constructor with all parameters.
    * @param domain_fqdn fqdn of domain added into auction
    * @param start_at the time when the auction is scheduled to start
    */
    explicit CreateAuction(
            std::string domain_fqdn,
            const std::chrono::system_clock::time_point& start_at);

    using Result = AuctionId;

    /**
    * Executes given operation
    * @param ctx contains reference to database transaction
    * @return id of the auction
    */
    Result exec(const OperationContext& ctx) const;
private:
    std::string domain_fqdn_;
    boost::optional<std::chrono::system_clock::time_point> start_at_;
};

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

#endif//CREATE_AUCTION_HH_8E3AB16B490FC3EF7157549FF287CEDE
