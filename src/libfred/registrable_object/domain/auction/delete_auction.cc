/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/registrable_object/domain/auction/delete_auction.hh"

#include "liblog/liblog.hh"

#include <utility>

namespace LibFred {
namespace Domain {
namespace Auction {

namespace {

template <typename Error, typename MsgGen>
[[noreturn]] void raise(MsgGen msg_gen)
{
    struct Exception : Error
    {
        Exception(MsgGen msg_gen) : msg_gen{std::move(msg_gen)} {}
        const char* what() const noexcept override { return msg_gen(); }
        MsgGen msg_gen;
    };
    throw Exception{std::move(msg_gen)};
}

}//namespace LibFred::Domain::Auction::{anonymous}

DeleteAuction::DeleteAuction(AuctionId auction_id)
    : auction_id_{auction_id}
{ }

DeleteAuction::Result DeleteAuction::exec(const OperationContext& ctx) const
{
    try
    {
        const auto dbres = ctx.get_conn().exec_params(
                "DELETE FROM domain_auction "
                 "WHERE id = $1::BIGINT "
             "RETURNING id",
                {Database::QueryParam{auction_id_}});
        if (dbres.size() <= 0)
        {
            raise<AuctionDoesNotExist>([]() { return "auction does not exist"; });
        }
        if (dbres.size() != 1)
        {
            raise<Exception>([]() { return "exactly one row expected"; });
        }
    }
    catch (const Exception& e)
    {
        LIBLOG_INFO("DeleteAuction::Exception caught: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
}

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred
