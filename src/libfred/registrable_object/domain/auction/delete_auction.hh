/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DELETE_AUCTION_HH_9D068858423705C1DBE11EBFF738BB30//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DELETE_AUCTION_HH_9D068858423705C1DBE11EBFF738BB30

#include "libfred/registrable_object/domain/auction/auction_id.hh"

#include "libfred/opcontext.hh"

#include <exception>
#include <string>

namespace LibFred {
namespace Domain {
namespace Auction {

/**
 * Delete auction.
 */
class DeleteAuction
{
public:
    struct Exception : std::exception {};
    struct AuctionDoesNotExist : Exception {};

    /**
    * Constructor with mandatory parameters.
    * @param auction_id auction id
    */
    DeleteAuction(AuctionId auction_id);

    using Result = void;

    /**
    * Executes given operation
    * @param ctx contains reference to database transaction
    */
    Result exec(const OperationContext& ctx) const;
private:
    AuctionId auction_id_;
};

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

#endif//DELETE_AUCTION_HH_9D068858423705C1DBE11EBFF738BB30
