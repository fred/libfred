/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/registrable_object/domain/auction/finish_auction.hh"

#include "liblog/liblog.hh"

#include <utility>

namespace LibFred {
namespace Domain {
namespace Auction {

namespace {

template <typename Error, typename MsgGen>
[[noreturn]] void raise(MsgGen msg_gen)
{
    struct Exception : Error
    {
        Exception(MsgGen msg_gen) : msg_gen{std::move(msg_gen)} {}
        const char* what() const noexcept override { return msg_gen(); }
        MsgGen msg_gen;
    };
    throw Exception{std::move(msg_gen)};
}

class Finish : boost::static_visitor<void>
{
public:
    explicit Finish(std::string& where, Database::QueryParams& params)
        : where_{where},
          params_{params}
    { }
    void operator()(AuctionId auction_id) const
    {
        params_.push_back(*auction_id);
        where_ = "id = $" + std::to_string(params_.size()) + "::BIGINT";
    }
    void operator()(const ExternalAuctionId& auction_id) const
    {
        params_.push_back(*auction_id);
        where_ = "external_id = $" + std::to_string(params_.size()) + "::TEXT";
    }
    void operator()(const Fqdn& fqdn) const
    {
        params_.push_back(*fqdn);
        where_ = "fqdn = LOWER($" + std::to_string(params_.size()) + "::TEXT)";
    }
private:
    std::string& where_;
    Database::QueryParams& params_;
};

}//namespace LibFred::Domain::Auction::{anonymous}

FinishAuction::FinishAuction(AuctionId auction_id)
    : auction_{auction_id}
{ }

FinishAuction::FinishAuction(ExternalAuctionId auction_id)
    : auction_{std::move(auction_id)}
{ }

FinishAuction::FinishAuction(Fqdn fqdn)
    : auction_{std::move(fqdn)}
{ }

FinishAuction::Result FinishAuction::exec(const OperationContext& ctx, Release when) const
{
    try
    {
        Database::QueryParams params;
        const auto make_sql_where_part = [&params, this]()
        {
            std::string sql;
            boost::apply_visitor(Finish{sql, params}, auction_);
            return sql;
        };
        const auto make_sql_set_part = [when]()
        {
            switch (when)
            {
                case Release::now:
                    return std::string{", released_at = NOW()"};
                case Release::later:
                    return std::string{};
            }
            struct UnknownReleaseValue : std::exception
            {
                const char* what() const noexcept override { return "unknown value of Release enum"; }
            };
            throw UnknownReleaseValue{};
        };
        const auto dbres = ctx.get_conn().exec_params(
                "UPDATE domain_auction "
                   "SET finished_at = NOW()" + make_sql_set_part() + " "
                 "WHERE finished_at IS NULL AND "
                       "released_at IS NULL AND " +
                        make_sql_where_part() + " "
             "RETURNING id",
                params);
        if (dbres.size() <= 0)
        {
            raise<AuctionDoesNotExist>([]() { return "auction does not exist"; });
        }
        if (dbres.size() != 1)
        {
            raise<Exception>([]() { return "exactly one row expected"; });
        }
    }
    catch (const Exception& e)
    {
        LIBLOG_INFO("FinishAuction::Exception caught: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
}

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred
