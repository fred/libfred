/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FINISH_AUCTION_HH_F7824187D8754EC0DB5541CBBC29BAD1//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define FINISH_AUCTION_HH_F7824187D8754EC0DB5541CBBC29BAD1

#include "libfred/registrable_object/domain/auction/auction_id.hh"

#include "libfred/opcontext.hh"

#include <boost/variant.hpp>

#include <exception>
#include <string>

namespace LibFred {
namespace Domain {
namespace Auction {

/**
 * Finish auction.
 */
class FinishAuction
{
public:
    struct Exception : std::exception {};
    struct AuctionDoesNotExist : Exception {};

    /**
    * Constructor with mandatory parameters.
    * @param auction_id auction id
    */
    FinishAuction(AuctionId auction_id);
    /**
    * Constructor with mandatory parameters.
    * @param auction_id external auction id
    */
    FinishAuction(ExternalAuctionId auction_id);
    /**
    * Constructor with mandatory parameters.
    * @param fqdn auctioned fqdn
    */
    FinishAuction(Fqdn fqdn);

    enum class Release
    {
        now,
        later
    };
    using Result = void;

    /**
    * Executes given operation
    * @param ctx contains reference to database transaction
    * @param when to release fqdn for registration
    */
    Result exec(const OperationContext& ctx, Release when = Release::now) const;
private:
    boost::variant<AuctionId, ExternalAuctionId, Fqdn> auction_;
};

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

#endif//FINISH_AUCTION_HH_F7824187D8754EC0DB5541CBBC29BAD1
