/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/registrable_object/domain/auction/get_auctions.hh"

#include "liblog/liblog.hh"

#include <algorithm>
#include <set>
#include <utility>

namespace LibFred {
namespace Domain {
namespace Auction {

GetAuctions::Result GetAuctions::exec(const OperationContext& ctx, std::initializer_list<Property> filter) const
{
    try
    {
        std::string sql = "SELECT da.id, "
                                 "da.uuid, "
                                 "COALESCE(da.external_id, ''), "
                                 "da.fqdn, "
                                 "da.external_id IS NULL, "
                                 "da.next_event_after < NOW() IS NOT TRUE AND da.winner_id IS NULL AND da.external_id IS NOT NULL, "
                                 "da.next_event_after < NOW() AND da.winner_id IS NULL AND da.external_id IS NOT NULL, "
                                 "NOW() < da.win_expires_at AND da.external_id IS NOT NULL, "
                                 "da.win_expires_at IS NULL AND da.winner_id IS NOT NULL AND da.external_id IS NOT NULL, "
                                 "da.win_expires_at <= NOW() AND da.external_id IS NOT NULL, "
                                 "MIN(obr.crdate) "
                            "FROM domain_auction da "
                       "LEFT JOIN object_registry obr ON obr.type = get_object_type_id('domain') AND "
                                                        "da.created_at <= obr.crdate AND "
                                                        "obr.name = da.fqdn AND "
                                                        "da.win_expires_at IS NULL AND "
                                                        "da.winner_id IS NOT NULL AND "
                                                        "da.external_id IS NOT NULL "
                           "WHERE da.created_at <= NOW() AND "
                                 "da.finished_at IS NULL";
        if (0 < filter.size())
        {
            const auto properties = std::set<Property>{begin(filter), end(filter)};
            std::string cond;
            std::for_each(begin(properties), end(properties), [&cond](auto&& property)
            {
                const auto extend_or_cond = [&cond](const char* expr)
                {
                    if (!cond.empty())
                    {
                        cond += " OR ";
                    }
                    cond += expr;
                };
                switch (property)
                {
                    case Property::without_external_auction_id:
                        return extend_or_cond("da.external_id IS NULL");
                    case Property::waiting_for_event:
                        return extend_or_cond("(da.next_event_after < NOW() IS NOT TRUE AND "
                                               "da.winner_id IS NULL AND "
                                               "da.external_id IS NOT NULL)");
                    case Property::event_anticipated:
                        return extend_or_cond("(da.next_event_after < NOW() AND "
                                               "da.winner_id IS NULL AND "
                                               "da.external_id IS NOT NULL)");
                    case Property::waiting_for_registration:
                        return extend_or_cond("(NOW() < da.win_expires_at AND "
                                               "da.external_id IS NOT NULL)");
                    case Property::registration_completed:
                        return extend_or_cond("(da.win_expires_at IS NULL AND "
                                               "da.winner_id IS NOT NULL AND "
                                               "da.external_id IS NOT NULL)");
                    case Property::registration_expired:
                        return extend_or_cond("(da.win_expires_at <= NOW() AND "
                                               "da.external_id IS NOT NULL)");
                }
                struct UnknownFilter : std::exception
                {
                    const char* what() const noexcept override { return "unknown filter"; }
                };
                throw UnknownFilter{};
            });
            sql += " AND (" + cond + ")";
        }
        sql += " GROUP BY 1 "
                "ORDER BY 1";
        const auto dbres = ctx.get_conn().exec(sql);
        Result result;
        result.reserve(dbres.size());
        for (auto idx = 0ull; idx < dbres.size(); ++idx)
        {
            const auto columns = dbres[idx];
            result.emplace_back(
                    AuctionId{static_cast<std::uint64_t>(columns[0])},
                    AuctionUuid{boost::uuids::string_generator{}(static_cast<std::string>(columns[1]))},
                    ExternalAuctionId{static_cast<std::string>(columns[2])},
                    static_cast<std::string>(columns[3]),
                    [&columns]()
                    {
                        if (static_cast<bool>(columns[4]))
                        {
                            return Property::without_external_auction_id;
                        }
                        if (static_cast<bool>(columns[5]))
                        {
                            return Property::waiting_for_event;
                        }
                        if (static_cast<bool>(columns[6]))
                        {
                            return Property::event_anticipated;
                        }
                        if (static_cast<bool>(columns[7]))
                        {
                            return Property::waiting_for_registration;
                        }
                        if (static_cast<bool>(columns[8]))
                        {
                            return Property::registration_completed;
                        }
                        if (static_cast<bool>(columns[9]))
                        {
                            return Property::registration_expired;
                        }
                        struct NoProperty : std::exception
                        {
                            const char* what() const noexcept override { return "no property"; }
                        };
                        throw NoProperty{};
                    }(),
                    columns[10].isnull() ? boost::none
                                         : boost::make_optional(static_cast<std::chrono::system_clock::time_point>(columns[10])));
        }
        return result;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
}

GetAuctions::AuctionInfo::AuctionInfo(
        AuctionId auction_id,
        AuctionUuid auction_uuid,
        ExternalAuctionId external_auction_id,
        std::string fqdn,
        Property property,
        boost::optional<std::chrono::system_clock::time_point> domain_crdate)
    : auction_id{auction_id},
      auction_uuid{std::move(auction_uuid)},
      external_auction_id{std::move(external_auction_id)},
      fqdn{std::move(fqdn)},
      property{property},
      domain_crdate{std::move(domain_crdate)}
{ }

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred
