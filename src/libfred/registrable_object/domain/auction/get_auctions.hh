/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_AUCTIONS_HH_D3FF177A99C8298E488E8C90E95383F0//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_AUCTIONS_HH_D3FF177A99C8298E488E8C90E95383F0

#include "libfred/registrable_object/domain/auction/auction_id.hh"

#include "libfred/opcontext.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <initializer_list>
#include <string>
#include <vector>

namespace LibFred {
namespace Domain {
namespace Auction {

/**
 * Get auctions with given properties.
 */
class GetAuctions
{
public:
    enum class Property
    {
        without_external_auction_id,
        waiting_for_event,
        event_anticipated,
        waiting_for_registration,
        registration_expired,
        registration_completed
    };

    struct AuctionInfo
    {
        explicit AuctionInfo(
                AuctionId auction_id,
                AuctionUuid auction_uuid,
                ExternalAuctionId external_auction_id,
                std::string fqdn,
                Property property,
                boost::optional<std::chrono::system_clock::time_point> domain_crdate = boost::none);
        AuctionId auction_id;
        AuctionUuid auction_uuid;
        ExternalAuctionId external_auction_id;
        std::string fqdn;
        Property property;
        boost::optional<std::chrono::system_clock::time_point> domain_crdate;
    };

    using Result = std::vector<AuctionInfo>;

    /**
    * Executes given operation
    * @param ctx contains reference to database transaction
    * @param filter auctions having given properties; empty filter means "all properties"
    * @return auctions with given properties
    */
    Result exec(const OperationContext& ctx, std::initializer_list<Property> filter = {}) const;
};

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

#endif//GET_AUCTIONS_HH_D3FF177A99C8298E488E8C90E95383F0
