/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/registrable_object/domain/auction/lock_auction.hh"

#include "liblog/liblog.hh"

namespace LibFred {
namespace Domain {
namespace Auction {

LockAuction::LockAuction(const OperationContext& ctx, const AuctionId& auction)
    : ctx_{ctx}
{
    const auto dbres = this->get_ctx().get_conn().exec_params(
            "SELECT "
              "FROM domain_auction "
             "WHERE id = $1::BIGINT "
        "FOR UPDATE",
            Database::QueryParams{*auction});
    if (dbres.size() <= 0)
    {
        struct NotFound : AuctionDoesNotExist
        {
            const char* what() const noexcept override { return "auction does not exist"; }
        };
        throw NotFound{};
    }
}

LockAuction::LockAuction(const OperationContext& ctx, const GetAuctions::AuctionInfo& auction)
    : ctx_{ctx}
{
    const auto dbres = this->get_ctx().get_conn().exec_params(
            "SELECT fqdn = $2::TEXT, "
                   "external_id IS NULL, "
                   "next_event_after < NOW() IS NOT TRUE AND winner_id IS NULL AND external_id IS NOT NULL, "
                   "next_event_after < NOW() AND winner_id IS NULL AND external_id IS NOT NULL, "
                   "NOW() < win_expires_at AND external_id IS NOT NULL, "
                   "win_expires_at IS NULL AND winner_id IS NOT NULL AND external_id IS NOT NULL, "
                   "win_expires_at <= NOW() AND external_id IS NOT NULL "
              "FROM domain_auction "
             "WHERE id = $1::BIGINT "
        "FOR UPDATE",
            Database::QueryParams{*auction.auction_id, auction.fqdn});
    if (dbres.size() <= 0)
    {
        struct NotFound : AuctionDoesNotExist
        {
            const char* what() const noexcept override { return "auction does not exist"; }
        };
        throw NotFound{};
    }
    if (1 < dbres.size())
    {
        struct TooManyRows : std::exception
        {
            const char* what() const noexcept override { return "more then one row selected"; }
        };
        throw TooManyRows{};
    }
    const auto column = dbres[0];
    if (!static_cast<bool>(column[0]))
    {
        struct DifferentFqdn : AuctionChanged
        {
            const char* what() const noexcept override { return "fqdn differs"; }
        };
        throw DifferentFqdn{};
    }
    const auto property = [&column]()
    {
        if (static_cast<bool>(column[1]))
        {
            return GetAuctions::Property::without_external_auction_id;
        }
        if (static_cast<bool>(column[2]))
        {
            return GetAuctions::Property::waiting_for_event;
        }
        if (static_cast<bool>(column[3]))
        {
            return GetAuctions::Property::event_anticipated;
        }
        if (static_cast<bool>(column[4]))
        {
            return GetAuctions::Property::waiting_for_registration;
        }
        if (static_cast<bool>(column[5]))
        {
            return GetAuctions::Property::registration_completed;
        }
        if (static_cast<bool>(column[6]))
        {
            return GetAuctions::Property::registration_expired;
        }
        struct NoProperty : AuctionChanged
        {
            const char* what() const noexcept override { return "no property"; }
        };
        throw NoProperty{};
    }();
    if (property != auction.property)
    {
        struct DifferentProperty : AuctionChanged
        {
            const char* what() const noexcept override { return "property differs"; }
        };
        throw DifferentProperty{};
    }
}

const OperationContext& LockAuction::get_ctx() const noexcept
{
    return ctx_;
}

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred
