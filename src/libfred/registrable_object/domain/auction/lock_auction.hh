/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOCK_AUCTION_HH_00601192CF777FA43E54F7A2F1407CD4//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LOCK_AUCTION_HH_00601192CF777FA43E54F7A2F1407CD4

#include "libfred/registrable_object/domain/auction/get_auctions.hh"

#include "libfred/opcontext.hh"

#include <exception>

namespace LibFred {
namespace Domain {
namespace Auction {

/**
 * Lock given auction to protect against concurrent updates.
 */
class LockAuction
{
public:
    struct Exception : std::exception {};
    struct AuctionDoesNotExist : Exception {};
    struct AuctionChanged : Exception {};
    /**
    * Lock given row for update
    * @param ctx contains reference to database transaction
    */
    explicit LockAuction(const OperationContext& ctx, const AuctionId& auction);
    /**
    * Lock given row for update and check for equality
    * @param ctx contains reference to database transaction
    */
    explicit LockAuction(const OperationContext& ctx, const GetAuctions::AuctionInfo& auction);
    const OperationContext& get_ctx() const noexcept;
private:
    const OperationContext& ctx_;
};

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

#endif//LOCK_AUCTION_HH_00601192CF777FA43E54F7A2F1407CD4
