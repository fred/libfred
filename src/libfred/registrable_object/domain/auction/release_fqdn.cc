/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/registrable_object/domain/auction/release_fqdn.hh"
#include "src/util/types/convert_sql_std_chrono_types.hh"

#include "liblog/liblog.hh"

namespace LibFred {
namespace Domain {
namespace Auction {

GetUnreleasedFqdn::Result GetUnreleasedFqdn::exec(const OperationContext& ctx) const
{
    try
    {
        const auto dbres = ctx.get_conn().exec(
                "SELECT id "
                  "FROM domain_auction "
                 "WHERE finished_at IS NOT NULL AND "
                       "released_at IS NULL "
              "ORDER BY 1");
        Result result;
        result.reserve(dbres.size());
        for (auto idx = 0ull; idx < dbres.size(); ++idx)
        {
            result.emplace_back(static_cast<std::uint64_t>(dbres[idx][0]));
        }
        return result;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
}

GetUnreleasedFqdn::Result GetUnreleasedFqdn::exec(
        const OperationContext& ctx,
        std::chrono::system_clock::time_point finished_before) const
{
    try
    {
        const auto dbres = ctx.get_conn().exec_params(
                "SELECT id "
                  "FROM domain_auction "
                 "WHERE finished_at < $1::TIMESTAMP AND "
                       "released_at IS NULL "
              "ORDER BY 1",
              {SqlConvert<std::chrono::system_clock::time_point>::convert_from_time_point(finished_before)});
        Result result;
        result.reserve(dbres.size());
        for (auto idx = 0ull; idx < dbres.size(); ++idx)
        {
            result.emplace_back(static_cast<std::uint64_t>(dbres[idx][0]));
        }
        return result;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
}

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

using namespace LibFred::Domain::Auction;

std::chrono::system_clock::time_point LibFred::Domain::Auction::release_fqdn(
        const OperationContext& ctx,
        AuctionId auction_id)
{
    try
    {
        const auto dbres = ctx.get_conn().exec_params(
                "UPDATE domain_auction "
                   "SET released_at = NOW() "
                 "WHERE id = $1::BIGINT AND "
                       "finished_at IS NOT NULL AND "
                       "released_at IS NULL "
             "RETURNING released_at",
                Database::QueryParams{*auction_id});
        if (dbres.size() == 1)
        {
            return static_cast<std::chrono::system_clock::time_point>(dbres[0][0]);
        }
        if (dbres.size() == 0)
        {
            struct NotFound : UnreleasedFqdnNotFound
            {
                const char* what() const noexcept override { return "unreleased fqdn not found"; }
            };
            throw NotFound{};
        }
        struct TooManyRows : std::exception
        {
            const char* what() const noexcept override { return "too many unreleased fqdn found"; }
        };
        throw TooManyRows{};
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
}
