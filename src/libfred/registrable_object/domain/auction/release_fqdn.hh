/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RELEASE_FQDN_HH_8829BE2CA74123C712CE94DDB120A8AA//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define RELEASE_FQDN_HH_8829BE2CA74123C712CE94DDB120A8AA

#include "libfred/registrable_object/domain/auction/auction_id.hh"

#include "libfred/opcontext.hh"

#include <chrono>
#include <exception>
#include <vector>

namespace LibFred {
namespace Domain {
namespace Auction {

/**
 * Get ids of finished auctions whose fqdn has not yet been released for registration.
 */
class GetUnreleasedFqdn
{
public:
    using Result = std::vector<AuctionId>;

    /**
    * Executes given operation
    * @param ctx contains reference to database transaction
    * @return auction ids waiting for release
    */
    Result exec(const OperationContext& ctx) const;

    /**
    * Executes given operation
    * @param ctx contains reference to database transaction
    * @param finished_before only fqdns with finished_at before this time
    * @return auction ids waiting for release
    */
    Result exec(const OperationContext& ctx, std::chrono::system_clock::time_point finished_before) const;
};

struct UnreleasedFqdnNotFound : std::exception { };

std::chrono::system_clock::time_point release_fqdn(const OperationContext& ctx, AuctionId auction_id);

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

#endif//RELEASE_FQDN_HH_8829BE2CA74123C712CE94DDB120A8AA
