/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/registrable_object/domain/auction/update_auction.hh"
#include "src/libfred/registrable_object/contact/info_contact.hh"
#include "src/util/types/convert_sql_std_chrono_types.hh"

#include "liblog/liblog.hh"

#include <numeric>
#include <utility>

namespace LibFred {
namespace Domain {
namespace Auction {

namespace {

template <typename Error, typename MsgGen>
[[noreturn]] void raise(MsgGen msg_gen)
{
    struct Exception : Error
    {
        Exception(MsgGen msg_gen) : msg_gen{std::move(msg_gen)} {}
        const char* what() const noexcept override { return msg_gen(); }
        MsgGen msg_gen;
    };
    throw Exception{std::move(msg_gen)};
}

class GetExistingWinnerId : public boost::static_visitor<std::uint64_t>
{
public:
    explicit GetExistingWinnerId(const OperationContext& ctx)
        : ctx_{ctx}
    { }
    std::uint64_t operator()(std::uint64_t winner_id) const
    {
        try
        {
            return InfoContactById{winner_id}.exec(ctx_).info_contact_data.id;
        }
        catch (const InfoContactById::Exception& e)
        {
            if (e.is_set_unknown_object_id())
            {
                raise<UpdateAuction::WinnerDoesNotExist>([]() { return "winner does not exist"; });
            }
            throw;
        }
    }
    std::uint64_t operator()(const boost::uuids::uuid& winner_uuid) const
    {
        try
        {
            const auto contact_uuid = Util::make_strong<RegistrableObject::Contact::ContactUuid>(winner_uuid);
            return InfoContactByUuid{contact_uuid}.exec(ctx_).info_contact_data.id;
        }
        catch (const InfoContactByUuid::Exception& e)
        {
            if (e.is_set_unknown_contact_uuid())
            {
                raise<UpdateAuction::WinnerDoesNotExist>([]() { return "winner does not exist"; });
            }
            throw;
        }
    }
private:
    OperationContextLockingForShare ctx_;
};

template <typename T>
bool to_update(const boost::optional<T>& o)
{
    return o != boost::none;
}

void push(Database::QueryParams& params, const boost::optional<std::chrono::system_clock::time_point>& t)
{
    if (t == boost::none)
    {
        params.emplace_back();
    }
    else
    {
        params.emplace_back(SqlConvert<std::chrono::system_clock::time_point>::convert_from_time_point(*t));
    }
}

template <typename ...Ts>
int number_of_updates(const boost::optional<Ts>& ...values)
{
    const auto int_values = {(to_update(values) ? 1 : 0)...};
    return std::accumulate(begin(int_values), end(int_values), 0);
}

}//namespace LibFred::Domain::Auction::{anonymous}

UpdateAuction::UpdateAuction(AuctionId auction_id)
    : auction_id_{auction_id},
      external_auction_id_{boost::none},
      created_at_{DoNotTouch{}},
      next_event_after_{boost::none},
      winner_id_{boost::none},
      win_expires_at_{boost::none}
{ }

UpdateAuction& UpdateAuction::set_external_auction_id(ExternalAuctionId external_auction_id)
{
    external_auction_id_ = boost::make_optional(*std::move(external_auction_id));
    return *this;
}

UpdateAuction& UpdateAuction::set_created_at(std::chrono::system_clock::time_point created_at)
{
    created_at_ = std::move(created_at);
    return *this;
}

UpdateAuction& UpdateAuction::set_created_at_to_now()
{
    created_at_ = Now{};
    return *this;
}

UpdateAuction& UpdateAuction::set_next_event_after(std::chrono::system_clock::time_point next_event_after)
{
    next_event_after_ = boost::make_optional(std::move(next_event_after));
    return *this;
}

UpdateAuction& UpdateAuction::set_next_event_after(std::nullptr_t)
{
    next_event_after_ = boost::make_optional(boost::optional<std::chrono::system_clock::time_point>{});
    return *this;
}

UpdateAuction& UpdateAuction::set_winner_id(std::uint64_t winner_id)
{
    winner_id_ = boost::make_optional(WinnerId(WinnerId::ValueType{winner_id}));
    return *this;
}

UpdateAuction& UpdateAuction::set_winner_id(boost::uuids::uuid winner_id)
{
    winner_id_ = boost::make_optional(WinnerId(WinnerId::ValueType{std::move(winner_id)}));
    return *this;
}

UpdateAuction& UpdateAuction::set_winner_id(std::nullptr_t)
{
    winner_id_ = boost::make_optional(WinnerId::Optional{});
    return *this;
}

UpdateAuction& UpdateAuction::set_win_expires_at(std::chrono::system_clock::time_point win_expires_at)
{
    win_expires_at_ = boost::make_optional(std::move(win_expires_at));
    return *this;
}

UpdateAuction& UpdateAuction::set_win_expires_at(std::nullptr_t)
{
    win_expires_at_ = boost::make_optional(boost::optional<std::chrono::system_clock::time_point>{});
    return *this;
}

UpdateAuction::Result UpdateAuction::exec(const OperationContext& ctx) const
{
    try
    {
        const auto update_created_at = [this]()
        {
            struct ToUpdate : boost::static_visitor<bool>
            {
                bool operator()(DoNotTouch) const noexcept
                {
                    return false;
                }
                bool operator()(Now) const noexcept
                {
                    return true;
                }
                bool operator()(const std::chrono::system_clock::time_point&) const noexcept
                {
                    return true;
                }
            };
            return boost::apply_visitor(ToUpdate{}, created_at_);
        }();
        const auto number_of_updated_items = number_of_updates(
                external_auction_id_,
                next_event_after_,
                winner_id_,
                win_expires_at_) + (update_created_at ? 1 : 0);
        if (number_of_updated_items == 0)
        {
            raise<MissingData>([]() { return "nothing to update"; });
        }
        Database::QueryParams params{};
        params.reserve(1 + number_of_updated_items);
        params.emplace_back(auction_id_);
        std::string sql_set_part = {};
        std::string sql_where_part = {};
        if (to_update(external_auction_id_))
        {
            params.emplace_back(*external_auction_id_);
            sql_set_part = "external_id = $2::TEXT";
            sql_where_part = " AND external_id IS NULL";
        }
        if (update_created_at)
        {
            class MakeSql : public boost::static_visitor<void>
            {
            public:
                explicit MakeSql(Database::QueryParams& params, std::string& sql_set_part)
                    : params_{params},
                      sql_set_part_{sql_set_part}
                { }
                void operator()(DoNotTouch) const
                {
                    throw DoNotTouch{};
                }
                void operator()(Now) const
                {
                    if (!sql_set_part_.empty())
                    {
                        sql_set_part_ += ", ";
                    }
                    sql_set_part_ += "created_at = LEAST(NOW(), created_at)";
                }
                void operator()(const std::chrono::system_clock::time_point& value) const
                {
                    if (!sql_set_part_.empty())
                    {
                        sql_set_part_ += ", ";
                    }
                    push(params_, value);
                    const auto new_created_at = [this]()
                    {
                        return "$" + std::to_string(params_.size()) + "::TIMESTAMP";
                    };
                    sql_set_part_ += "created_at = CASE WHEN NOW() < created_at "
                                                       "THEN GREATEST(NOW(), " + new_created_at() + ") "
                                                       "ELSE created_at "
                                                  "END";
                }
            private:
                Database::QueryParams& params_;
                std::string& sql_set_part_;
            };
            boost::apply_visitor(MakeSql{params, sql_set_part}, created_at_);
        }
        if (to_update(next_event_after_))
        {
            if (!sql_set_part.empty())
            {
                sql_set_part += ", ";
            }
            push(params, *next_event_after_);
            sql_set_part += "next_event_after = $" + std::to_string(params.size()) + "::TIMESTAMP WITHOUT TIME ZONE";
        }
        if (to_update(winner_id_))
        {
            if (!sql_set_part.empty())
            {
                sql_set_part += ", ";
            }
            if (*winner_id_ == WinnerId::nullopt)
            {
                sql_set_part += "winner_id = NULL";
            }
            else
            {
                const auto contact_id = boost::apply_visitor(GetExistingWinnerId{ctx}, ***winner_id_);
                params.emplace_back(contact_id);
                sql_set_part += "winner_id = $" + std::to_string(params.size()) + "::BIGINT";
            }
        }
        if (to_update(win_expires_at_))
        {
            if (!sql_set_part.empty())
            {
                sql_set_part += ", ";
            }
            push(params, *win_expires_at_);
            sql_set_part += "win_expires_at = $" + std::to_string(params.size()) + "::TIMESTAMP WITHOUT TIME ZONE";
        }
        if (!update_created_at)
        {
            sql_where_part += " AND created_at <= NOW()";
        }
        const auto dbres = ctx.get_conn().exec_params(
                "WITH u AS "
                "("
                    "UPDATE domain_auction "
                       "SET " + sql_set_part + " "
                     "WHERE id = $1::BIGINT AND "
                           "finished_at IS NULL" + sql_where_part + " "
                 "RETURNING id"
                ")"
                "SELECT (SELECT COUNT(*) FROM u) = 1, created_at <= NOW() "
                  "FROM domain_auction "
                 "WHERE id = $1::BIGINT",
                params);
        if (dbres.size() <= 0)
        {
            raise<AuctionDoesNotExist>([]() { return "auction does not exist"; });
        }
        if (dbres.size() != 1)
        {
            raise<Exception>([]() { return "exactly one row expected"; });
        }
        if (!update_created_at)
        {
            const auto scheduled_auction = !static_cast<bool>(dbres[0][1]);
            if (scheduled_auction)
            {
                raise<ScheduledAuction>([]() { return "can not update scheduled auction"; });
            }
        }
        if (to_update(external_auction_id_))
        {
            const auto already_set = !static_cast<bool>(dbres[0][0]);
            if (already_set)
            {
                raise<ExternalAuctionIdAlreadySet>([]() { return "auction id already set"; });
            }
        }
    }
    catch (const Exception& e)
    {
        LIBLOG_INFO("UpdateAuction::Exception caught: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
}

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

using namespace LibFred::Domain::Auction;

void LibFred::Domain::Auction::fqdn_registered(const OperationContext& ctx, const std::string& fqdn)
{
    try
    {
        const auto dbres = ctx.get_conn().exec_params(
                "UPDATE domain_auction "
                   "SET win_expires_at = NULL "
                 "WHERE fqdn = LOWER($1::TEXT) AND "
                       "winner_id IS NOT NULL AND "
                       "win_expires_at IS NOT NULL AND "
                       "finished_at IS NULL "
             "RETURNING id, winner_id",
            {Database::QueryParam{fqdn}});
        if (dbres.size() == 0)
        {
            LIBLOG_INFO("fqdn {} not auctioned", fqdn);
            return;
        }
        if (dbres.size() == 1)
        {
            LIBLOG_DEBUG("fqdn {} from auction {} registered by {}",
                         fqdn,
                         static_cast<std::uint64_t>(dbres[0][0]),
                         static_cast<std::uint64_t>(dbres[0][1]));
            return;
        }
    }
    catch (const Database::Exception& e)
    {
        LIBLOG_INFO("Database::Exception caught: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
        throw;
    }
    throw std::runtime_error{"too many rows returned"};
}
