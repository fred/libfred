/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_AUCTION_HH_6A669A3124D8BE252D940D85BDB52AFB//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_AUCTION_HH_6A669A3124D8BE252D940D85BDB52AFB

#include "libfred/registrable_object/domain/auction/auction_id.hh"

#include "libfred/opcontext.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <exception>
#include <string>

namespace LibFred {
namespace Domain {
namespace Auction {

/**
 * Update auction data.
 */
class UpdateAuction
{
public:
    struct Exception : std::exception {};
    struct AuctionDoesNotExist : Exception {};
    struct WinnerDoesNotExist : Exception {};
    struct ExternalAuctionIdAlreadySet : Exception {};
    struct MissingData : Exception {};
    struct ScheduledAuction : Exception {};

    /**
    * Constructor with mandatory parameter.
    * @param auction_id id of the auction
    */
    explicit UpdateAuction(AuctionId auction_id);

    /**
    * Sets id of the auction.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_external_auction_id(ExternalAuctionId external_auction_id);

    /**
    * Sets created_at if it currently has a value set in the future.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_created_at(std::chrono::system_clock::time_point created_at);

    /**
    * Sets created_at to now if it currently has a value set in the future.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_created_at_to_now();

    /**
    * Sets next_event_after.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_next_event_after(std::chrono::system_clock::time_point next_event_after);
    /**
    * Sets next_event_after to NULL.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_next_event_after(std::nullptr_t = nullptr);

    /**
    * Sets winner_id.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_winner_id(std::uint64_t winner_id);
    /**
    * Sets winner_id.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_winner_id(boost::uuids::uuid winner_id);
    /**
    * Sets winner_id to NULL.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_winner_id(std::nullptr_t = nullptr);

    /**
    * Sets expires_at.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_win_expires_at(std::chrono::system_clock::time_point win_expires_at);
    /**
    * Sets expires_at to NULL.
    * @return operation instance reference to allow method chaining
    */
    UpdateAuction& set_win_expires_at(std::nullptr_t = nullptr);

    using Result = void;

    /**
    * Executes given operation
    * @param ctx contains reference to database transaction
    */
    Result exec(const OperationContext& ctx) const;
private:
    struct DoNotTouch { };
    struct Now { };
    AuctionId auction_id_;
    boost::optional<ExternalAuctionId::ValueType> external_auction_id_;
    boost::variant<DoNotTouch, Now, std::chrono::system_clock::time_point> created_at_;
    boost::optional<boost::optional<std::chrono::system_clock::time_point>> next_event_after_;
    boost::optional<WinnerId::Optional> winner_id_;
    boost::optional<boost::optional<std::chrono::system_clock::time_point>> win_expires_at_;
};

void fqdn_registered(const OperationContext& ctx, const std::string& fqdn);

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

#endif//UPDATE_AUCTION_HH_6A669A3124D8BE252D940D85BDB52AFB
