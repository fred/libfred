/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "libfred/registrable_object/domain/check_domain.hh"
#include "libfred/registrable_object/domain/domain_name.hh"
#include "libfred/zone/zone.hh"
#include "libfred/object/object.hh"

#include "libfred/db_settings.hh"

#include <boost/uuid/string_generator.hpp>

namespace LibFred {

namespace {

Database::QueryParam to_query_param(const std::chrono::system_clock::time_point& t)
{
    return Database::QueryParam{SqlConvert<std::chrono::system_clock::time_point>::convert_from_time_point(t)};
}

}//namespace LibFred::{anonymous}

    CheckDomain::CheckDomain(const std::string& fqdn, const bool _is_system_registrar)
    : fqdn_(fqdn), is_system_registrar_(_is_system_registrar)
    {}

    bool CheckDomain::is_invalid_syntax(const OperationContext& ctx) const
    {
        try
        {
            if (!Domain::is_rfc1123_compliant_host_name(fqdn_)) {
                return true;
            }

            //remove optional root dot from fqdn
            std::string no_root_dot_fqdn = LibFred::Zone::rem_trailing_dot(fqdn_);

            //get zone
            Zone::Data zone;
            try
            {
                zone = Zone::find_zone_in_fqdn(ctx, no_root_dot_fqdn);
            }
            catch (const Zone::Exception& ex)
            {
                if (ex.is_set_unknown_zone_in_fqdn()
                        && (ex.get_unknown_zone_in_fqdn().compare(no_root_dot_fqdn) == 0))
                {
                    return true;//zone not found
                }
                else {
                    throw;
                }
            }

            //domain_name_validation
            if (!LibFred::Domain::DomainNameValidator(is_system_registrar_)
                .set_checker_names(LibFred::Domain::get_domain_name_validation_config_for_zone(ctx, zone.name))
                .set_zone_name(LibFred::Domain::DomainName(zone.name))
                .set_ctx(ctx)
                .exec(LibFred::Domain::DomainName(fqdn_), std::count(zone.name.begin(), zone.name.end(), '.') + 1) // skip zone labels
            )
            {
                return true;
            }
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }
        return false;//meaning ok
    }

    bool CheckDomain::is_bad_zone(const OperationContext& ctx) const
    {
        try
        {
            //remove optional root dot from fqdn
            std::string no_root_dot_fqdn = LibFred::Zone::rem_trailing_dot(fqdn_);

            //zone
            Zone::Data zone;
            try
            {
                zone = Zone::find_zone_in_fqdn(ctx, no_root_dot_fqdn);
            }
            catch (const Zone::Exception& ex)
            {
                if (ex.is_set_unknown_zone_in_fqdn()
                        && (ex.get_unknown_zone_in_fqdn().compare(no_root_dot_fqdn) == 0))
                {
                    return true;
                }
                else {
                    throw;
                }
            }
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }

        return false;//meaning ok
    }

    bool CheckDomain::is_bad_length(const OperationContext& ctx) const
    {
        try
        {
            //remove optional root dot from fqdn
            std::string no_root_dot_fqdn = LibFred::Zone::rem_trailing_dot(fqdn_);

            //get zone
            Zone::Data zone;
            try
            {
                zone = Zone::find_zone_in_fqdn(ctx, no_root_dot_fqdn);
            }
            catch (const Zone::Exception& ex)
            {
                if (ex.is_set_unknown_zone_in_fqdn()
                        && (ex.get_unknown_zone_in_fqdn().compare(no_root_dot_fqdn) == 0))
                {
                    return true;//zone not found
                }
                else {
                    throw;
                }
            }

            //check number of labels
            if (std::count(no_root_dot_fqdn.begin(), no_root_dot_fqdn.end(), '.')+1//fqdn labels number
                > std::count(zone.name.begin(), zone.name.end(), '.')+1+zone.dots_max)//max labels by zone
            {
                return true;
            }
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }
        return false;//meaning ok
    }

    bool CheckDomain::is_blacklisted(const OperationContext& ctx) const
    {
        try
        {
            //remove optional root dot from fqdn
            std::string no_root_dot_fqdn = LibFred::Zone::rem_trailing_dot(fqdn_);

            Database::Result bl_res  = ctx.get_conn().exec_params(
                    // clang-format off
                "SELECT id FROM domain_blacklist "
                 "WHERE ((NOT is_regex AND LOWER($1::TEXT) = fqdn) "
                        "OR (is_regex AND $1::TEXT ~* fqdn)) "
                   "AND valid_from <= NOW() "
                   "AND (valid_to IS NULL OR NOW() < valid_to) "
                    // clang-format on
            , Database::query_param_list(no_root_dot_fqdn));
            if (bl_res.size() > 0)//positively blacklisted
            {
                return true;
            }
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }
        return false;//meaning ok
    }

    std::set<boost::uuids::uuid> CheckDomain::blacklisted_by(
            const OperationContext& ctx,
            const boost::optional<std::chrono::system_clock::time_point>& time) const
    {
        std::set<boost::uuids::uuid> result;
        try
        {
            //remove optional root dot from fqdn
            std::string no_root_dot_fqdn = LibFred::Zone::rem_trailing_dot(fqdn_);

            std::string query{
                    // clang-format off
                    "SELECT uuid "
                      "FROM domain_blacklist "
                     "WHERE ((NOT is_regex AND LOWER($1::TEXT) = fqdn) "
                            "OR (is_regex AND $1::TEXT ~* fqdn)) "};
                    // clang-format on
            Database::QueryParams params{no_root_dot_fqdn};
            if (time == boost::none)
            {
                query += "AND valid_from <= NOW() "
                         "AND (valid_to IS NULL OR NOW() < valid_to)";
            }
            else
            {
                query += "AND valid_from <= $2::TIMESTAMP WITHOUT TIME ZONE "
                         "AND (valid_to IS NULL OR $2::TIMESTAMP WITHOUT TIME ZONE < valid_to)",
                        params.push_back(to_query_param(*time));
            }
            auto dbres = ctx.get_conn().exec_params(query, params);
            for (std::size_t idx = 0; idx < dbres.size(); ++idx)
            {
                result.insert(boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
            }
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }
        return result;
    }

    bool CheckDomain::is_blacklisted_at(
            const OperationContext& ctx,
            const boost::optional<std::chrono::system_clock::time_point>& time) const
    {
        if (time == boost::none)
        {
            return is_blacklisted(ctx);
        }
        try
        {
            //remove optional root dot from fqdn
            std::string no_root_dot_fqdn = LibFred::Zone::rem_trailing_dot(fqdn_);

            return 0 < ctx.get_conn().exec_params(
                    // clang-format off
               "SELECT FROM domain_blacklist "
                "WHERE ((NOT is_regex AND LOWER($1::TEXT) = fqdn) "
                       "OR (is_regex AND $1::TEXT ~* fqdn)) "
                  "AND valid_from <= $2::TIMESTAMP WITHOUT TIME ZONE "
                  "AND (valid_to IS NULL OR $2::TIMESTAMP WITHOUT TIME ZONE < valid_to)",
                    // clang-format on
               Database::QueryParams{no_root_dot_fqdn, to_query_param(*time)}).size();
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }
    }

    bool CheckDomain::will_be_blacklisted_after(
            const OperationContext& ctx,
            const boost::optional<std::chrono::system_clock::time_point>& time) const
    {
        try
        {
            //remove optional root dot from fqdn
            std::string no_root_dot_fqdn = LibFred::Zone::rem_trailing_dot(fqdn_);

            if (time == boost::none)
            {
                return 0 < ctx.get_conn().exec_params(
                    // clang-format off
                    "SELECT "
                      "FROM domain_blacklist "
                     "WHERE ((NOT is_regex AND LOWER($1::TEXT) = fqdn) "
                            "OR (is_regex AND $1::TEXT ~* fqdn)) "
                       "AND valid_from <= NOW() "
                       "AND (valid_to IS NULL OR NOW() < valid_to)",
                    // clang-format on
                    Database::QueryParams{no_root_dot_fqdn}).size();
            }
            return 0 < ctx.get_conn().exec_params(
                    // clang-format off
                "SELECT "
                  "FROM domain_blacklist "
                 "WHERE ((NOT is_regex AND LOWER($1::TEXT) = fqdn) "
                        "OR (is_regex AND $1::TEXT ~* fqdn)) "
                   "AND valid_from <= $2::TIMESTAMP WITHOUT TIME ZONE "
                   "AND (valid_to IS NULL OR $2::TIMESTAMP WITHOUT TIME ZONE < valid_to)",
                    // clang-format on
                Database::QueryParams{no_root_dot_fqdn, to_query_param(*time)}).size();
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }
    }

    bool CheckDomain::is_registered(const OperationContext& ctx, std::string& conflicting_fqdn_out) const
    {
        try
        {
            //remove optional root dot from fqdn
            std::string no_root_dot_fqdn = LibFred::Zone::rem_trailing_dot(fqdn_);

            //get zone
            Zone::Data zone;
            try
            {
                zone = Zone::find_zone_in_fqdn(ctx, no_root_dot_fqdn);
            }
            catch (const Zone::Exception& ex)
            {
                if (ex.is_set_unknown_zone_in_fqdn()
                        && (ex.get_unknown_zone_in_fqdn().compare(no_root_dot_fqdn) == 0))
                {
                    return false;//zone not found
                }
                else {
                    throw;
                }
            }

            if (zone.is_enum)
            {
                Database::Result conflicting_fqdn_res  = ctx.get_conn().exec_params(
                        "SELECT obr.name, obr.id "
                        "FROM object_registry obr "
                        "JOIN domain d ON d.id = obr.id " // helps to dramatically reduce the number of candidate objects!!!
                        "WHERE obr.type = get_object_type_id('domain'::TEXT) AND "
                              "obr.erdate IS NULL AND "
                              "(($1::TEXT LIKE ('%.' || obr.name)) OR "
                               "(obr.name LIKE ('%.' || $1::TEXT)) OR "
                               "(obr.name = LOWER($1::TEXT))) AND "
                              "d.zone = $2::INT "
                        "LIMIT 1",
                        Database::query_param_list(no_root_dot_fqdn)(zone.id));
                if (conflicting_fqdn_res.size() > 0)//have conflicting_fqdn
                {
                    conflicting_fqdn_out = static_cast<std::string>(conflicting_fqdn_res[0][0]);
                    return true;
                }
            }
            else
            {//is not ENUM
                Database::Result conflicting_fqdn_res  = ctx.get_conn().exec_params(
                    "SELECT o.name, o.id FROM object_registry o WHERE o.type=get_object_type_id('domain'::text) "
                    "AND o.erdate ISNULL AND o.name=LOWER($1::text) LIMIT 1"
                , Database::query_param_list(no_root_dot_fqdn));
                if (conflicting_fqdn_res.size() > 0)//have conflicting_fqdn
                {
                    conflicting_fqdn_out = static_cast<std::string>(conflicting_fqdn_res[0][0]);
                    return true;
                }

            }
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }
        return false;//meaning ok
    }

    bool CheckDomain::is_registered(const OperationContext& ctx) const
    {
        std::string conflicting_fqdn_out;
        return is_registered(ctx, conflicting_fqdn_out);
    }

    bool CheckDomain::is_auctioned(const OperationContext& ctx) const
    {
        try
        {
            return 0 < ctx.get_conn().exec_params(
                "SELECT "
                  "FROM domain_auction "
                 "WHERE fqdn = LOWER($1::TEXT) AND "
                       "created_at <= NOW() AND "
                       "released_at IS NULL",
                 {Database::QueryParam{fqdn_}}).size();
        }
        catch (ExceptionStack& e)
        {
            e.add_exception_stack_info(to_string());
            throw;
        }
    }

    bool CheckDomain::is_registerable_by_winner(const OperationContext& ctx) const
    {
        try
        {
            return 0 < ctx.get_conn().exec_params(
                "SELECT "
                  "FROM domain_auction "
                 "WHERE fqdn = LOWER($1::TEXT) AND "
                       "created_at <= NOW() AND "
                       "finished_at IS NULL AND "
                       "released_at IS NULL AND "
                       "winner_id IS NOT NULL AND "
                       "NOW() < win_expires_at",
                 {Database::QueryParam{fqdn_}}).size();
        }
        catch (ExceptionStack& e)
        {
            e.add_exception_stack_info(to_string());
            throw;
        }
    }

    bool CheckDomain::is_winner(const OperationContext& ctx, const std::string& registrant) const
    {
        try
        {
            return 0 < ctx.get_conn().exec_params(
                "SELECT "
                  "FROM domain_auction "
                 "WHERE fqdn = LOWER($1::TEXT) AND "
                       "created_at <= NOW() AND "
                       "finished_at IS NULL AND "
                       "released_at IS NULL AND "
                       "winner_id = (SELECT id FROM object_registry "
                                             "WHERE erdate IS NULL AND "
                                                   "type = get_object_type_id('contact') AND "
                                                   "UPPER(name) = UPPER($2::TEXT)) AND "
                       "NOW() < win_expires_at",
                 {Database::QueryParam{fqdn_}, Database::QueryParam{registrant}}).size();
        }
        catch (ExceptionStack& e)
        {
            e.add_exception_stack_info(to_string());
            throw;
        }
    }

    bool CheckDomain::is_available(const OperationContext& ctx) const
    {
        try
        {
            if (is_invalid_syntax(ctx)
            || is_bad_length(ctx)
            || is_registered(ctx)
            || is_blacklisted(ctx)
            || is_auctioned(ctx))
            {
                return false;
            }
        }
        catch (ExceptionStack& ex)
        {
            ex.add_exception_stack_info(to_string());
            throw;
        }
        return true;//meaning ok
    }

    std::string CheckDomain::to_string() const
    {
        return Util::format_operation_state("CheckDomain",
        Util::vector_of<std::pair<std::string, std::string> >
        (std::make_pair("fqdn", fqdn_))
        );
    }

} // namespace LibFred

