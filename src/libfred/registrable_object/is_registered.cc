/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/registrable_object/is_registered.hh"

#include "src/libfred/opexception.hh"

#include <boost/throw_exception.hpp>

namespace LibFred {

bool is_registered_by_uuid(
        const OperationContext& _ctx,
        Object_Type::Enum _object_type,
        const boost::uuids::uuid& _object_uuid)
{
    const auto object_type_name = Conversion::Enums::to_db_handle(_object_type);
    auto dbres = _ctx.get_conn().exec_params(
            // clang-format off
            "SELECT "
              "FROM object_registry obr "
             "WHERE obr.type = get_object_type_id($2::TEXT) AND "
                   "obr.erdate IS NULL AND "
                   "obr.uuid = $1::UUID",
            Database::query_param_list(_object_uuid)
                                      (object_type_name));
            // clang-format on

    if (dbres.size() > 1)
    {
        BOOST_THROW_EXCEPTION(InternalError("query result size > 1"));
    }
    return dbres.size() == 1;
}

bool is_registered_by_history_uuid(
        const OperationContext& _ctx,
        Object_Type::Enum _object_type,
        const boost::uuids::uuid& _object_history_uuid)
{
    const auto object_type_name = Conversion::Enums::to_db_handle(_object_type);
    auto dbres = _ctx.get_conn().exec_params(
            // clang-format off
            "SELECT "
              "FROM object_registry obr "
              "JOIN history h ON h.id = obr.historyid "
             "WHERE obr.type = get_object_type_id($2::TEXT) "
               "AND obr.erdate IS NULL "
               "AND h.uuid = $1::UUID",
            Database::query_param_list(_object_history_uuid)
                                      (object_type_name));
            // clang-format on
    if (dbres.size() > 1)
    {
        BOOST_THROW_EXCEPTION(InternalError("query result size > 1"));
    }
    return dbres.size() == 1;
}

} // namespace LibFred
