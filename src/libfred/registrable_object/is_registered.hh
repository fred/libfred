/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IS_REGISTERED_HH_61CEDB94F1DE4837BC16C12AD05BD1F8
#define IS_REGISTERED_HH_61CEDB94F1DE4837BC16C12AD05BD1F8

#include "libfred/object/object_type.hh"
#include "libfred/opcontext.hh"

#include <boost/uuid/uuid.hpp>

namespace LibFred {

bool is_registered_by_uuid(
        const OperationContext& _ctx,
        Object_Type::Enum _object_type,
        const boost::uuids::uuid& _object_uuid);

bool is_registered_by_history_uuid(
        const OperationContext& _ctx,
        Object_Type::Enum _object_type,
        const boost::uuids::uuid& _object_history_uuid);

} // namespace LibFred

#endif
