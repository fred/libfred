/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_REFERENCE_HH_6F08393EBC044C629F6CF8262D8E36CC
#define OBJECT_REFERENCE_HH_6F08393EBC044C629F6CF8262D8E36CC

#include "libfred/object/object_type.hh"
#include "libfred/registrable_object/uuid.hh"

#include "util/printable.hh"

#include <string>

namespace LibFred {
namespace RegistrableObject {

/**
 * RegistrableObject identification structure.
 */
template <Object_Type::Enum object_type>
struct RegistrableObjectReference : Util::Printable<RegistrableObjectReference<object_type>>
{
    /**
    * Default constructor of data structure.
    */
    explicit RegistrableObjectReference();

    RegistrableObjectReference(const RegistrableObjectReference&) = default;
    RegistrableObjectReference(RegistrableObjectReference&&) = default;
    RegistrableObjectReference& operator=(const RegistrableObjectReference&) = default;
    RegistrableObjectReference& operator=(RegistrableObjectReference&&) = default;

    /**
    * Constructor of data structure.
    * @param id sets object database id into @ref id attribute
    * @param handle sets object handle into @ref handle attribute
    * @param uuid sets object uuid into @ref uuid attribute
    */
    explicit RegistrableObjectReference(
            unsigned long long id,
            std::string handle,
            const RegistrableObject::UuidOf<object_type>& uuid);

    /**
     * Comparison operator comparing all attributes.
     * Handle is compared case insensitive
     * @param rhs data compared with this instance
     */
    bool operator==(const RegistrableObjectReference& rhs) const;

    /**
     * Comparison operator comparing all attributes.
     * Handle is compared case insensitive
     * @param rhs data compared with this instance
     */
    bool operator!=(const RegistrableObjectReference& rhs) const;

    /**
     * Comparison operator comparing all attributes
     * Handle is compared case insensitive
     * @param rhs data compared with this instance
     */
    bool operator<(const RegistrableObjectReference& rhs) const;

    /**
    * Dumps state of the instance into the string
    * @return string with description of the instance state
    */
    std::string to_string() const;

    unsigned long long id; /**< database id of the object */
    std::string handle; /**< handle of the object */
    RegistrableObject::UuidOf<object_type> uuid; /**< UUID of the object */
};

/**
 * RegistrableObject specialization for contact, extended by `name` and `organization` attributes.
 */
template <>
struct RegistrableObjectReference<Object_Type::contact> : Util::Printable<RegistrableObjectReference<Object_Type::contact>>
{
    /**
    * Default constructor of data structure.
    */
    explicit RegistrableObjectReference();

    RegistrableObjectReference(const RegistrableObjectReference&) = default;
    RegistrableObjectReference(RegistrableObjectReference&&) = default;
    RegistrableObjectReference& operator=(const RegistrableObjectReference&) = default;
    RegistrableObjectReference& operator=(RegistrableObjectReference&&) = default;

    /**
    * Constructor of data structure.
    * @param id sets object database id into @ref id attribute
    * @param handle sets object handle into @ref handle attribute
    * @param uuid sets object uuid into @ref uuid attribute
    */
    explicit RegistrableObjectReference(
            unsigned long long id,
            std::string handle,
            const RegistrableObject::UuidOf<Object_Type::contact>& uuid,
            std::string name,
            std::string organization);

    /**
     * Comparison operator comparing all attributes.
     * Handle is compared case insensitive
     * @param rhs data compared with this instance
     */
    bool operator==(const RegistrableObjectReference& rhs) const;

    /**
     * Comparison operator comparing all attributes.
     * Handle is compared case insensitive
     * @param rhs data compared with this instance
     */
    bool operator!=(const RegistrableObjectReference& rhs) const;

    /**
     * Comparison operator comparing all attributes
     * Handle is compared case insensitive
     * @param rhs data compared with this instance
     */
    bool operator<(const RegistrableObjectReference& rhs) const;

    /**
    * Dumps state of the instance into the string
    * @return string with description of the instance state
    */
    std::string to_string() const;

    unsigned long long id; /**< database id of the contact */
    std::string handle; /**< handle of the contact */
    RegistrableObject::UuidOf<Object_Type::contact> uuid; /**< UUID of the contact */
    std::string name; /**< name of the contact */
    std::string organization; /**< organization of the contact */
};

} // namespace LibFred::RegistrableObject
} // namespace LibFred

#endif
