/*
 * Copyright (C) 2025  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libfred/registrable_object/snapshot_id.hh"
#include "util/log/log.hh"

#include <boost/uuid/uuid_io.hpp>

#include <cstring>
#include <sstream>
#include <utility>

namespace LibFred {
namespace RegistrableObject {

namespace {

template <DbLock> struct ToSqlLockString;

template <>
struct ToSqlLockString<DbLock::for_share>
{
    std::string operator()() const
    {
        return "SHARE";
    }
};

template <>
struct ToSqlLockString<DbLock::for_update>
{
    std::string operator()() const
    {
        return "UPDATE";
    }
};

template <typename Type, typename Tag>
std::string as_string(const Util::StrongType<Type, Tag>& value)
{
    std::ostringstream out;
    out << get_raw_value_from(value);
    return out.str();
}

constexpr bool is_sign(char c)
{
    switch (c)
    {
        case '+': return true;
        case '-': return true;
        default: return false;
    }
}

constexpr bool is_digit(char c)
{
    return ('0' <= c) && (c <= '9');
}

constexpr bool is_digit_at_begin(const char* begin, const char* end)
{
    return (begin != end) && is_digit(*begin);
}

constexpr bool is_decimal_point_at_begin(const char* begin, const char* end)
{
    return (begin != end) && (*begin == '.');
}

// snapshot_id is a decimal number representing number of seconds since unix epoch
constexpr const char* check_snapshot_id_validity(const char* const begin, const char* const end)
{
    if (begin == end)
    {
        return "snapshot_id can't be empty";
    }
    bool has_whole_number_part = false;
    bool has_decimal_number_part = false;
    const char* c_ptr = begin;
    if (is_sign(*c_ptr))
    {
        ++c_ptr;
    }
    while (is_digit_at_begin(c_ptr, end))
    {
        has_whole_number_part = true;
        ++c_ptr;
    }
    if (is_decimal_point_at_begin(c_ptr, end))
    {
        ++c_ptr;
        while (is_digit_at_begin(c_ptr, end))
        {
            has_decimal_number_part = true;
            ++c_ptr;
        }
    }
    if (c_ptr != end)
    {
        return "snapshot_id contains an invalid character";
    }
    if (!has_whole_number_part &&
        !has_decimal_number_part)
    {
        return "missing whole and decimal number parts of snapshot_id";
    }
    return nullptr;
}

constexpr bool is_snapshot_id_valid(const char* const c_str)
{
    return check_snapshot_id_validity(c_str, c_str + std::strlen(c_str)) == nullptr;
}

static_assert(is_snapshot_id_valid("1234567890.0123456789"), "must be valid");
static_assert(is_snapshot_id_valid("+3.7"), "must be valid");
static_assert(is_snapshot_id_valid("-3.7"), "must be valid");
static_assert(is_snapshot_id_valid("3"), "must be valid");
static_assert(is_snapshot_id_valid("3."), "must be valid");
static_assert(is_snapshot_id_valid("+3"), "must be valid");
static_assert(is_snapshot_id_valid("+3."), "must be valid");
static_assert(is_snapshot_id_valid("-3"), "must be valid");
static_assert(is_snapshot_id_valid("-3."), "must be valid");
static_assert(is_snapshot_id_valid(".7"), "must be valid");
static_assert(is_snapshot_id_valid("+.7"), "must be valid");
static_assert(is_snapshot_id_valid("-.7"), "must be valid");

static_assert(!is_snapshot_id_valid(""), "must be invalid");
static_assert(!is_snapshot_id_valid("+"), "must be invalid");
static_assert(!is_snapshot_id_valid("-"), "must be invalid");
static_assert(!is_snapshot_id_valid("."), "must be invalid");
static_assert(!is_snapshot_id_valid("+."), "must be invalid");
static_assert(!is_snapshot_id_valid("-."), "must be invalid");
static_assert(!is_snapshot_id_valid("a"), "must be invalid");
static_assert(!is_snapshot_id_valid("a0"), "must be invalid");
static_assert(!is_snapshot_id_valid("0a"), "must be invalid");

void check_snapshot_id_validity(const SnapshotId& snapshot_id)
{
    const auto& str = get_raw_value_from(snapshot_id);
    const char* const reason = check_snapshot_id_validity(str.c_str(), str.c_str() + str.length());
    if (reason != nullptr)
    {
        struct InvalidSnapshotIdString : InvalidSnapshotId
        {
            explicit InvalidSnapshotIdString(const char* msg) : message{msg} {}
            const char* what() const noexcept override { return message; }
            const char* message;
        };
        throw InvalidSnapshotIdString{reason};
    }
}

}//namespace LibFred::RegistrableObject::{anonymous}

CheckObjectChange::CheckObjectChange(ObjectUuid uuid)
    : uuid_{std::move(uuid)}
{}

template <Object_Type::Enum o>
CheckObjectChange::CheckObjectChange(UuidOf<o> uuid)
    : uuid_{Util::make_strong<ObjectUuid>(get_raw_value_from(uuid))}
{}

template <DbLock lock>
CheckObjectChange::Result CheckObjectChange::exec(const OperationContextUsing<lock>& ctx_lock, const SnapshotId& snapshot_id) const
{
    FREDLOG_DEBUG(boost::format{"%1%, snapshot_id: %2%"} % this->to_string() % get_raw_value_from(snapshot_id));
    check_snapshot_id_validity(snapshot_id);
    const auto& ctx = static_cast<const OperationContext&>(ctx_lock);
    const auto dbres = ctx.get_conn().exec_params(
            "WITH o AS " // object
            "("
                "SELECT obr.id, h.valid_from, h.valid_to "
                  "FROM object_registry obr "
                  "JOIN history h ON h.id = obr.historyid "
                 "WHERE obr.uuid = $1::UUID AND obr.erdate IS NULL "
                   "FOR " + ToSqlLockString<lock>{}() + " OF obr"
            "), "
            "cs AS " // current state
            "("
                "SELECT ARRAY_AGG(os.id ORDER BY os.id) AS state "
                  "FROM o "
             "LEFT JOIN object_state os ON os.object_id = o.id AND "
                                          "os.valid_to IS NULL "
              "GROUP BY o.id"
            "), "
            "hs AS " // history state
            "("
                "SELECT ARRAY_AGG(os.id ORDER BY os.id) AS state "
                  "FROM o "
             "LEFT JOIN object_state os ON os.object_id = o.id AND "
                                          "os.valid_from <= TO_TIMESTAMP($2::NUMERIC) AND "
                                          "COALESCE(TO_TIMESTAMP($2::NUMERIC) < os.valid_to, TRUE) "
              "GROUP BY o.id"
            ") "
            "SELECT o.id, "
                   "o.valid_from <= TO_TIMESTAMP($2::NUMERIC) AND o.valid_to IS NULL, "
                   "cs.state = hs.state "
              "FROM o, cs, hs",
            Database::QueryParams{
                as_string(uuid_),
                get_raw_value_from(snapshot_id)
            });
    if (dbres.size() <= 0)
    {
        FREDLOG_INFO("object not found");
        struct ObjectDoesNotExist : UnknownObjectId
        {
            const char* what() const noexcept override { return "object does not exist"; }
        };
        throw ObjectDoesNotExist{};
    }
    if (1 < dbres.size())
    {
        FREDLOG_WARNING("too many objects");
        struct TooManyObjects : Exception
        {
            const char* what() const noexcept override { return "too many objects"; }
        };
        throw TooManyObjects{};
    }
    const auto result = Result{
            .data_has_been_changed = !static_cast<bool>(dbres[0][1]),
            .state_has_been_changed = !static_cast<bool>(dbres[0][2])
    };
    FREDLOG_DEBUG(boost::format{"{id: %1%, data_has_been_changed: %2%, state_has_been_changed: %3%}"} %
                                static_cast<unsigned long long>(dbres[0][0]) %
                                result.data_has_been_changed %
                                result.state_has_been_changed);
    return result;
}

std::string CheckObjectChange::to_string() const
{
    return Util::format_operation_state(
            "CheckObjectChange",
            {
                std::make_pair(std::string{"uuid"}, as_string(uuid_))
            });
}

template CheckObjectChange::CheckObjectChange<Object_Type::contact>(UuidOf<Object_Type::contact>);
template CheckObjectChange::CheckObjectChange<Object_Type::domain>(UuidOf<Object_Type::domain>);
template CheckObjectChange::CheckObjectChange<Object_Type::keyset>(UuidOf<Object_Type::keyset>);
template CheckObjectChange::CheckObjectChange<Object_Type::nsset>(UuidOf<Object_Type::nsset>);

template CheckObjectChange::Result CheckObjectChange::exec<DbLock::for_share>(const OperationContextUsing<DbLock::for_share>&, const SnapshotId&) const;
template CheckObjectChange::Result CheckObjectChange::exec<DbLock::for_update>(const OperationContextUsing<DbLock::for_update>&, const SnapshotId&) const;

}//namespace LibFred::RegistrableObject
}//namespace LibFred

using namespace LibFred::RegistrableObject;

SnapshotId LibFred::RegistrableObject::get_snapshot_id(const OperationContext& ctx)
{
    const auto now = static_cast<std::string>(ctx.get_conn().exec("SELECT EXTRACT(EPOCH FROM NOW())")[0][0]);
    return Util::make_strong<SnapshotId>(now);
}
