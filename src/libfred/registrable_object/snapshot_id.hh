/*
 * Copyright (C) 2025  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SNAPSHOT_ID_HH_491371EB7F0A985DC22F2D57AAD4C352//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define SNAPSHOT_ID_HH_491371EB7F0A985DC22F2D57AAD4C352

#include "libfred/exception.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrable_object/uuid.hh"

#include "util/printable.hh"

#include <string>

namespace LibFred {
namespace RegistrableObject {

using SnapshotId = Util::StrongType<std::string, struct SnapshotIdTag>;

SnapshotId get_snapshot_id(const OperationContext&);

struct InvalidSnapshotId : Exception { };

class CheckObjectChange : public Util::Printable<CheckObjectChange>
{
public:
    explicit CheckObjectChange(ObjectUuid);
    template <Object_Type::Enum o>
    explicit CheckObjectChange(UuidOf<o>);
    struct Result
    {
        bool data_has_been_changed;
        bool state_has_been_changed;
    };
    template <DbLock lock>
    Result exec(const OperationContextUsing<lock>&, const SnapshotId&) const;
    std::string to_string() const;
private:
    ObjectUuid uuid_;
};

}//namespace LibFred::RegistrableObject
}//namespace LibFred

#endif//SNAPSHOT_ID_HH_491371EB7F0A985DC22F2D57AAD4C352
