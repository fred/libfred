/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libfred/registrar/create_registrar.hh"
#include "libfred/registrable_object/contact/create_contact.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/contact/delete_contact.hh"
#include "libfred/registrable_object/domain/auction/create_auction.hh"
#include "libfred/registrable_object/domain/auction/delete_auction.hh"
#include "libfred/registrable_object/domain/auction/finish_auction.hh"
#include "libfred/registrable_object/domain/auction/get_auctions.hh"
#include "libfred/registrable_object/domain/auction/lock_auction.hh"
#include "libfred/registrable_object/domain/auction/release_fqdn.hh"
#include "libfred/registrable_object/domain/auction/update_auction.hh"
#include "libfred/registrable_object/domain/check_domain.hh"
#include "libfred/registrable_object/domain/create_domain.hh"
#include "libfred/registrable_object/domain/create_domain_name_blacklist_id.hh"
#include "libfred/registrable_object/domain/delete_domain.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "liblog/liblog.hh"
#include "util/types/convert_sql_std_chrono_types.hh"
#include "test/setup/fixtures.hh"

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <string>
#include <tuple>

namespace LibFred {
namespace Domain {
namespace Auction {

std::ostream& operator<<(std::ostream& out, GetAuctions::Property property)
{
    return out << [property]()
    {
        switch (property)
        {
            case GetAuctions::Property::without_external_auction_id:
                return "without_external_auction_id";
            case GetAuctions::Property::waiting_for_event:
                return "waiting_for_event";
            case GetAuctions::Property::event_anticipated:
                return "event_anticipated";
            case GetAuctions::Property::waiting_for_registration:
                return "waiting_for_registration";
            case GetAuctions::Property::registration_completed:
                return "registration_completed";
            case GetAuctions::Property::registration_expired:
                return "registration_expired";
        }
        return "";
    }();
}

namespace {

std::string to_string(const boost::optional<std::chrono::system_clock::time_point>& t)
{
    if (t == boost::none)
    {
        return "none";
    }
    return SqlConvert<std::chrono::system_clock::time_point>::to(*t);
}

}//namespace LibFred::Domain::Auction::{anonymous}

std::ostream& operator<<(std::ostream& out, const GetAuctions::AuctionInfo& info)
{
    return out << "{" << *info.auction_id << ", "
                      << *info.auction_uuid << ", "
                      << *info.external_auction_id << ", "
                      << info.fqdn << ", "
                      << info.property << ", "
                      << to_string(info.domain_crdate)
               << "}";
}

bool operator==(const GetAuctions::AuctionInfo& lhs, const GetAuctions::AuctionInfo& rhs)
{
    return std::make_tuple(*lhs.auction_id, *lhs.auction_uuid, *lhs.external_auction_id, lhs.fqdn, lhs.property, lhs.domain_crdate) ==
           std::make_tuple(*rhs.auction_id, *rhs.auction_uuid, *rhs.external_auction_id, rhs.fqdn, rhs.property, rhs.domain_crdate);
}

}//namespace LibFred::Domain::Auction
}//namespace LibFred::Domain
}//namespace LibFred

namespace {

void enable_auction(const LibFred::OperationContext& ctx, const char* zone_fqdn)
{
    BOOST_REQUIRE_NO_THROW([&]()
    {
        const auto dbres = ctx.get_conn().exec_params(
                "UPDATE zone "
                   "SET auction_enabled = true "
                 "WHERE fqdn = $1::TEXT "
             "RETURNING 0",
                {Database::QueryParam{zone_fqdn}});
        BOOST_REQUIRE_EQUAL(dbres.size(), 1);
    }());
}

std::chrono::system_clock::time_point one_hour_after(const LibFred::OperationContext& ctx)
{
    std::chrono::system_clock::time_point result;
    BOOST_REQUIRE_NO_THROW([&]()
    {
        const auto t = static_cast<std::string>(ctx.get_conn().exec("SELECT (NOW() + '1HOUR'::INTERVAL)::TIMESTAMP")[0][0]);
        result = SqlConvert<std::chrono::system_clock::time_point>::from(t);
    }());
    return result;
}

std::chrono::system_clock::time_point one_minute_before(const LibFred::OperationContext& ctx)
{
    std::chrono::system_clock::time_point result;
    BOOST_REQUIRE_NO_THROW([&]()
    {
        const auto t = static_cast<std::string>(ctx.get_conn().exec("SELECT (NOW() - '1MINUTE'::INTERVAL)::TIMESTAMP")[0][0]);
        result = SqlConvert<std::chrono::system_clock::time_point>::from(t);
    }());
    return result;
}

}//namespace {anonymous}

struct DomainAuctionFixture : Test::instantiate_db_template
{
    DomainAuctionFixture()
        : registrar_handle{"REG-COMMON"},
          contact_handle{"DOMAIN-OWNER"},
          domain_fqdn{"auctioned.cz"}
    {
        ::LibFred::OperationContextCreator ctx;
        ::LibFred::CreateRegistrar{
                registrar_handle,
                "Common",
                "Common ltd.",
                {"No street"},
                "Praha",
                "CZ",
                "reg@common.cz",
                "https://common.cz",
                "12344321"}.exec(ctx);

        ::LibFred::Contact::PlaceAddress place;
        place.street1 = std::string("STR1");
        place.city = "Praha";
        place.postalcode = "11150";
        place.country = "CZ";
        ::LibFred::CreateContact{contact_handle, registrar_handle}
            .set_name("Domain Owner")
            .set_disclosename(true)
            .set_place(place)
            .set_discloseaddress(true)
            .exec(ctx);
        const auto contact = ::LibFred::InfoContactByHandle{contact_handle}.exec(ctx).info_contact_data;
        winner_id = contact.id;
        winner_uuid = get_raw_value_from(contact.uuid);
        ::LibFred::CreateDomain{domain_fqdn, registrar_handle, contact_handle}.exec(ctx);
        ctx.commit_transaction();
    }
    std::string registrar_handle;
    std::string contact_handle;
    unsigned long long winner_id;
    boost::uuids::uuid winner_uuid;
    std::string domain_fqdn;
};

BOOST_FIXTURE_TEST_SUITE(TestDomainAuction, DomainAuctionFixture)

BOOST_AUTO_TEST_CASE(AuctionHappyPath)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
    BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_registerable_by_winner(ctx));
    BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_winner(ctx, contact_handle));
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_REQUIRE_EQUAL(GetAuctions{}.exec(ctx).size(), 0);
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    BOOST_CHECK_LT(AuctionId{0ull}, auction_id);
    BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
    BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_registerable_by_winner(ctx));
    BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_winner(ctx, contact_handle));

    static constexpr auto external_auction_id = "my auction";
    const auto to_set_id = GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id});
    BOOST_REQUIRE_EQUAL(to_set_id.size(), 1);
    BOOST_CHECK_EQUAL(to_set_id[0], (GetAuctions::AuctionInfo{
            auction_id,
            to_set_id[0].auction_uuid,
            ExternalAuctionId{std::string{}},
            domain_fqdn,
            GetAuctions::Property::without_external_auction_id}));
    UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{external_auction_id}).exec(ctx);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id}).size(), 0);

    const auto next_event_after_one_hour = one_hour_after(ctx);
    UpdateAuction{auction_id}.set_next_event_after(next_event_after_one_hour).exec(ctx);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::event_anticipated}).size(), 0);

    const auto next_event_before_one_minute = one_minute_before(ctx);
    UpdateAuction{auction_id}.set_next_event_after(next_event_before_one_minute).exec(ctx);
    const auto event_anticipated = GetAuctions{}.exec(ctx, {GetAuctions::Property::event_anticipated});
    BOOST_REQUIRE_EQUAL(event_anticipated.size(), 1);
    BOOST_CHECK_EQUAL(event_anticipated[0], (GetAuctions::AuctionInfo{
            auction_id,
            to_set_id[0].auction_uuid,
            ExternalAuctionId{std::string{external_auction_id}},
            domain_fqdn,
            GetAuctions::Property::event_anticipated}));
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed,
                                               GetAuctions::Property::registration_expired}).size(), 0);

    UpdateAuction{auction_id}.set_winner_id(winner_id)
                             .set_win_expires_at(next_event_after_one_hour).exec(ctx);
    ::LibFred::CreateDomain{domain_fqdn, registrar_handle, contact_handle}.exec(ctx);
    const auto domain_crdate = SqlConvert<std::chrono::system_clock::time_point>::convert_to_time_point(
            LibFred::InfoDomainByFqdn{domain_fqdn}.exec(ctx, "UTC").info_domain_data.creation_time);
    BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
    BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_registerable_by_winner(ctx));
    BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_winner(ctx, contact_handle));
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed,
                                               GetAuctions::Property::registration_expired}).size(), 0);
    {
        ::LibFred::Domain::Auction::fqdn_registered(ctx, domain_fqdn);
        BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_registerable_by_winner(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_winner(ctx, contact_handle));
        const auto completed = GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed});
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 0);
        BOOST_REQUIRE_EQUAL(completed.size(), 1);
        BOOST_CHECK_EQUAL(completed[0], (GetAuctions::AuctionInfo{
                auction_id,
                to_set_id[0].auction_uuid,
                ExternalAuctionId{std::string{external_auction_id}},
                domain_fqdn,
                GetAuctions::Property::registration_completed,
                domain_crdate}));
    }
    {
        UpdateAuction{auction_id}.set_winner_id(winner_id)
                                 .set_win_expires_at().exec(ctx);
        BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_registerable_by_winner(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_winner(ctx, contact_handle));
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 0);
        {
            const auto completed = GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed});
            BOOST_REQUIRE_EQUAL(completed.size(), 1);
            BOOST_CHECK_EQUAL(completed[0], (GetAuctions::AuctionInfo{
                auction_id,
                to_set_id[0].auction_uuid,
                ExternalAuctionId{std::string{external_auction_id}},
                domain_fqdn,
                GetAuctions::Property::registration_completed,
                domain_crdate}));
        }
        {
            const auto completed = GetAuctions{}.exec(ctx);
            BOOST_REQUIRE_EQUAL(completed.size(), 1);
            BOOST_CHECK_EQUAL(completed[0], (GetAuctions::AuctionInfo{
                auction_id,
                to_set_id[0].auction_uuid,
                ExternalAuctionId{std::string{external_auction_id}},
                domain_fqdn,
                GetAuctions::Property::registration_completed,
                domain_crdate}));
        }
        {
            const auto completed = GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id,
                                                            GetAuctions::Property::event_anticipated,
                                                            GetAuctions::Property::registration_completed,
                                                            GetAuctions::Property::registration_expired});
            BOOST_REQUIRE_EQUAL(completed.size(), 1);
            BOOST_CHECK_EQUAL(completed[0], (GetAuctions::AuctionInfo{
                auction_id,
                to_set_id[0].auction_uuid,
                ExternalAuctionId{std::string{external_auction_id}},
                domain_fqdn,
                GetAuctions::Property::registration_completed,
                domain_crdate}));
        }
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id,
                                                   GetAuctions::Property::event_anticipated,
                                                   GetAuctions::Property::registration_expired}).size(), 0);
    }
    {
        UpdateAuction{auction_id}.set_winner_id(winner_uuid)
                                 .set_win_expires_at().exec(ctx);
        BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_registerable_by_winner(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_winner(ctx, contact_handle));
        const auto completed = GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed});
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 0);
        BOOST_REQUIRE_EQUAL(completed.size(), 1);
        BOOST_CHECK_EQUAL(completed[0], (GetAuctions::AuctionInfo{
            auction_id,
            to_set_id[0].auction_uuid,
            ExternalAuctionId{std::string{external_auction_id}},
            domain_fqdn,
            GetAuctions::Property::registration_completed,
            domain_crdate}));
    }
    {
        UpdateAuction{auction_id}.set_winner_id(winner_id)
                                 .set_win_expires_at(next_event_before_one_minute).exec(ctx);
        BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_registerable_by_winner(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_winner(ctx, contact_handle));
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed}).size(), 0);
        const auto expired = GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired});
        BOOST_REQUIRE_EQUAL(expired.size(), 1);
        BOOST_CHECK_EQUAL(expired[0], (GetAuctions::AuctionInfo{
            auction_id,
            to_set_id[0].auction_uuid,
            ExternalAuctionId{std::string{external_auction_id}},
            domain_fqdn,
            GetAuctions::Property::registration_expired}));
    }
    {
        UpdateAuction{auction_id}.set_winner_id(winner_uuid)
                                 .set_win_expires_at(next_event_before_one_minute).exec(ctx);
        BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_registerable_by_winner(ctx));
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_winner(ctx, contact_handle));
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed}).size(), 0);
        const auto expired = GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired});
        BOOST_REQUIRE_EQUAL(expired.size(), 1);
        BOOST_CHECK_EQUAL(expired[0], (GetAuctions::AuctionInfo{
            auction_id,
            to_set_id[0].auction_uuid,
            ExternalAuctionId{std::string{external_auction_id}},
            domain_fqdn,
            GetAuctions::Property::registration_expired}));
    }
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::event_anticipated}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 1);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id}).size(), 0);
    BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
    FinishAuction{auction_id}.exec(ctx, FinishAuction::Release::later);
    BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
    BOOST_CHECK_EXCEPTION(
            FinishAuction{auction_id}.exec(ctx),
            FinishAuction::AuctionDoesNotExist,
            [](auto&& e)
            {
                LIBLOG_INFO("FinishAuction::AuctionDoesNotExist exception caught: {}", e.what());
                return true;
            });
    const auto unreleased = GetUnreleasedFqdn{}.exec(ctx);
    BOOST_REQUIRE_EQUAL(unreleased.size(), 1);
    BOOST_CHECK_NO_THROW(release_fqdn(ctx, unreleased[0]));
    BOOST_CHECK_EXCEPTION(
            release_fqdn(ctx, unreleased[0]),
            UnreleasedFqdnNotFound,
            [](auto&& e)
            {
                LIBLOG_INFO("UnreleasedFqdnNotFound exception caught: {}", e.what());
                return true;
            });
    DeleteAuction{auction_id}.exec(ctx);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::event_anticipated}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id}).size(), 0);
}

BOOST_AUTO_TEST_CASE(DomainRegistered)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_CHECK_EXCEPTION(
            CreateAuction{domain_fqdn}.exec(ctx),
            CreateAuction::DomainRegistered,
            [](auto&& e)
            {
                LIBLOG_INFO("CreateAuction::DomainRegistered exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(FqdnAlreadyInAuction)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_REQUIRE_NO_THROW(CreateAuction{domain_fqdn}.exec(ctx));
    BOOST_CHECK_EXCEPTION(
            CreateAuction{domain_fqdn}.exec(ctx),
            CreateAuction::FqdnAlreadyInAuction,
            [](auto&& e)
            {
                LIBLOG_INFO("CreateAuction::FqdnAlreadyInAuction exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(AuctionDisabled)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    using namespace ::LibFred::Domain::Auction;
    BOOST_CHECK_EXCEPTION(
            CreateAuction{domain_fqdn}.exec(ctx),
            CreateAuction::AuctionDisabled,
            [](auto&& e)
            {
                LIBLOG_INFO("CreateAuction::AuctionDisabled exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(InvalidFqdnSyntax)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_CHECK_EXCEPTION(
            CreateAuction{"-" + domain_fqdn}.exec(ctx),
            CreateAuction::InvalidFqdnSyntax,
            [](auto&& e)
            {
                LIBLOG_INFO("CreateAuction::InvalidFqdnSyntax exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(FqdnBlacklisted)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    const auto domain = ::LibFred::InfoDomainByFqdn{domain_fqdn}.exec(ctx).info_domain_data;
    ::LibFred::CreateDomainNameBlacklistId{domain.id, "test"}.exec(ctx);
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_CHECK_EXCEPTION(
            CreateAuction{domain_fqdn}.exec(ctx),
            CreateAuction::FqdnBlacklisted,
            [](auto&& e)
            {
                LIBLOG_INFO("CreateAuction::FqdnBlacklisted exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(AuctionDoesNotExist)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_CHECK_EXCEPTION(
            UpdateAuction{AuctionId{1ull}}.set_winner_id(winner_id).exec(ctx),
            UpdateAuction::AuctionDoesNotExist,
            [](auto&& e)
            {
                LIBLOG_INFO("UpdateAuction::AuctionDoesNotExist exception caught: {}", e.what());
                return true;
            });
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    BOOST_CHECK_EXCEPTION(
            UpdateAuction{auction_id + 1}.set_winner_id(winner_id).exec(ctx),
            UpdateAuction::AuctionDoesNotExist,
            [](auto&& e)
            {
                LIBLOG_INFO("UpdateAuction::AuctionDoesNotExist exception caught: {}", e.what());
                return true;
            });
    BOOST_CHECK_EXCEPTION(
            DeleteAuction{auction_id + 1}.exec(ctx),
            DeleteAuction::AuctionDoesNotExist,
            [](auto&& e)
            {
                LIBLOG_INFO("DeleteAuction::AuctionDoesNotExist exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(WinnerIdDoesNotExist)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    BOOST_CHECK_EXCEPTION(
            UpdateAuction{auction_id}.set_winner_id(winner_id + 1).exec(ctx),
            UpdateAuction::WinnerDoesNotExist,
            [](auto&& e)
            {
                LIBLOG_INFO("UpdateAuction::WinnerDoesNotExist exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(WinnerUuidDoesNotExist)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    static const auto any_uuid = boost::uuids::string_generator{}("26d5cd5a-b31a-4888-b41a-a40b1e04dee4");
    BOOST_CHECK_EXCEPTION(
            UpdateAuction{auction_id}.set_winner_id(any_uuid).exec(ctx),
            UpdateAuction::WinnerDoesNotExist,
            [](auto&& e)
            {
                LIBLOG_INFO("UpdateAuction::WinnerDoesNotExist exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(MissingData)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    BOOST_CHECK_EXCEPTION(
            UpdateAuction{auction_id}.exec(ctx),
            UpdateAuction::MissingData,
            [](auto&& e)
            {
                LIBLOG_INFO("UpdateAuction::MissingData exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(ExternalAuctionIdAlreadySet)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    static constexpr auto eai = "first auction";
    BOOST_REQUIRE_NO_THROW(UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{eai}).exec(ctx));
    BOOST_CHECK_EXCEPTION(
            UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{eai}).exec(ctx),
            UpdateAuction::ExternalAuctionIdAlreadySet,
            [](auto&& e)
            {
                LIBLOG_INFO("UpdateAuction::ExternalAuctionIdAlreadySet exception caught: {}", e.what());
                return true;
            });
    BOOST_CHECK_EXCEPTION(
            UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{"second auction"}).exec(ctx),
            UpdateAuction::ExternalAuctionIdAlreadySet,
            [](auto&& e)
            {
                LIBLOG_INFO("UpdateAuction::ExternalAuctionIdAlreadySet exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(DeleteWinner)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    static constexpr auto external_auction_id = "my auction";
    UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{external_auction_id}).exec(ctx);
    const auto next_event_after_one_hour = one_hour_after(ctx);
    UpdateAuction{auction_id}.set_next_event_after(next_event_after_one_hour)
                             .set_winner_id(winner_id)
                             .set_win_expires_at(next_event_after_one_hour).exec(ctx);
    BOOST_REQUIRE_NO_THROW(::LibFred::DeleteContactByHandle{contact_handle}.exec(ctx));
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::event_anticipated}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 1);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id}).size(), 0);
    DeleteAuction{auction_id}.exec(ctx);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::event_anticipated}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_completed}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::registration_expired}).size(), 0);
    BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id}).size(), 0);
}

BOOST_AUTO_TEST_CASE(ScheduledAuction)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    const auto start_in_the_future = one_hour_after(ctx);
    const auto start_in_the_past = one_minute_before(ctx);
    {
        const auto auction_id = CreateAuction{domain_fqdn, start_in_the_future}.exec(ctx);
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx).size(), 0);
        static constexpr auto external_auction_id = "my auction";
        BOOST_CHECK_EXCEPTION(
                UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{external_auction_id}).exec(ctx),
                UpdateAuction::ScheduledAuction,
                [](auto&& e)
                {
                    LIBLOG_INFO("UpdateAuction::ScheduledAuction exception caught: {}", e.what());
                    return true;
                });
        BOOST_CHECK_NO_THROW(UpdateAuction{auction_id}.set_created_at(start_in_the_future + std::chrono::hours{24}).exec(ctx));
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx).size(), 0);
        BOOST_CHECK_EXCEPTION(
                UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{external_auction_id}).exec(ctx),
                UpdateAuction::ScheduledAuction,
                [](auto&& e)
                {
                    LIBLOG_INFO("UpdateAuction::ScheduledAuction exception caught: {}", e.what());
                    return true;
                });
        BOOST_CHECK_NO_THROW(UpdateAuction{auction_id}.set_created_at(start_in_the_past).exec(ctx));
        const auto auction_uuid = [&]()
        {
            const auto auctions = GetAuctions{}.exec(ctx);
            BOOST_REQUIRE_EQUAL(auctions.size(), 1);
            BOOST_CHECK_EQUAL(auctions[0], (GetAuctions::AuctionInfo{
                auction_id,
                auctions[0].auction_uuid,
                ExternalAuctionId{std::string{}},
                domain_fqdn,
                GetAuctions::Property::without_external_auction_id}));
            return auctions[0].auction_uuid;
        }();
        BOOST_CHECK_NO_THROW(UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{external_auction_id}).exec(ctx));
        {
            const auto waiting = GetAuctions{}.exec(ctx);
            BOOST_REQUIRE_EQUAL(waiting.size(), 1);
            BOOST_CHECK_EQUAL(waiting[0], (GetAuctions::AuctionInfo{
                auction_id,
                auction_uuid,
                ExternalAuctionId{external_auction_id},
                domain_fqdn,
                GetAuctions::Property::waiting_for_event}));
        }
        BOOST_CHECK_NO_THROW(UpdateAuction{auction_id}.set_next_event_after(start_in_the_future).exec(ctx));
        {
            const auto waiting = GetAuctions{}.exec(ctx);
            BOOST_REQUIRE_EQUAL(waiting.size(), 1);
            BOOST_CHECK_EQUAL(waiting[0], (GetAuctions::AuctionInfo{
                auction_id,
                auction_uuid,
                ExternalAuctionId{external_auction_id},
                domain_fqdn,
                GetAuctions::Property::waiting_for_event}));
        }
        BOOST_CHECK_NO_THROW(UpdateAuction{auction_id}.set_next_event_after(start_in_the_past).exec(ctx));
        {
            const auto auctions = GetAuctions{}.exec(ctx);
            BOOST_REQUIRE_EQUAL(auctions.size(), 1);
            BOOST_CHECK_EQUAL(auctions[0], (GetAuctions::AuctionInfo{
                auction_id,
                auction_uuid,
                ExternalAuctionId{std::string{external_auction_id}},
                domain_fqdn,
                GetAuctions::Property::event_anticipated}));
        }
        DeleteAuction{auction_id}.exec(ctx);
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx).size(), 0);
    }
    {
        const auto auction_id = CreateAuction{domain_fqdn, start_in_the_future}.exec(ctx);
        BOOST_CHECK_EQUAL(GetAuctions{}.exec(ctx).size(), 0);
        static constexpr auto external_auction_id = "my auction";
        BOOST_CHECK_EXCEPTION(
                UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{external_auction_id}).exec(ctx),
                UpdateAuction::ScheduledAuction,
                [](auto&& e)
                {
                    LIBLOG_INFO("UpdateAuction::ScheduledAuction exception caught: {}", e.what());
                    return true;
                });
        BOOST_CHECK_NO_THROW(UpdateAuction{auction_id}.set_created_at_to_now().exec(ctx));
        {
            const auto auctions = GetAuctions{}.exec(ctx);
            BOOST_REQUIRE_EQUAL(auctions.size(), 1);
            BOOST_CHECK_EQUAL(auctions[0], (GetAuctions::AuctionInfo{
                auction_id,
                auctions[0].auction_uuid,
                ExternalAuctionId{std::string{}},
                domain_fqdn,
                GetAuctions::Property::without_external_auction_id}));
        }
        BOOST_CHECK(::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
        FinishAuction{auction_id}.exec(ctx, FinishAuction::Release::now);
        BOOST_CHECK(!::LibFred::CheckDomain{domain_fqdn}.is_auctioned(ctx));
    }
}

BOOST_AUTO_TEST_CASE(LockAuctionById)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_REQUIRE_EQUAL(GetAuctions{}.exec(ctx).size(), 0);
    BOOST_CHECK_EXCEPTION(
            (LockAuction{ctx, AuctionId{0ull}}),
            LockAuction::AuctionDoesNotExist,
            [](auto&& e)
            {
                LIBLOG_INFO("LockAuction::AuctionDoesNotExist exception caught: {}", e.what());
                return true;
            });
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    const auto unset_external_id = GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id});
    BOOST_REQUIRE_EQUAL(unset_external_id.size(), 1);
    {
        BOOST_CHECK_NO_THROW((LockAuction{ctx, unset_external_id.front().auction_id}));
    }
}

BOOST_AUTO_TEST_CASE(LockAuctionByAuctionInfo)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_REQUIRE_EQUAL(GetAuctions{}.exec(ctx).size(), 0);
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    const auto unset_external_id = GetAuctions{}.exec(ctx, {GetAuctions::Property::without_external_auction_id});
    BOOST_REQUIRE_EQUAL(unset_external_id.size(), 1);
    LockAuction lock{ctx, unset_external_id.front()};
    static constexpr auto external_auction_id = "my auction";
    UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{external_auction_id}).exec(lock.get_ctx());
    BOOST_CHECK_EXCEPTION(
            (LockAuction{ctx, unset_external_id.front()}),
            LockAuction::AuctionChanged,
            [](auto&& e)
            {
                LIBLOG_INFO("LockAuction::AuctionChanged exception caught: {}", e.what());
                return true;
            });
    const auto non_existent_auction = [&unset_external_id]()
    {
        auto auction = unset_external_id.front();
        auction.auction_id = AuctionId{0ull};
        return auction;
    }();
    BOOST_CHECK_EXCEPTION(
            (LockAuction{ctx, non_existent_auction}),
            LockAuction::AuctionDoesNotExist,
            [](auto&& e)
            {
                LIBLOG_INFO("LockAuction::AuctionDoesNotExist exception caught: {}", e.what());
                return true;
            });
}

BOOST_AUTO_TEST_CASE(GetUnreleasedFqdnFinishedBefore)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    ::LibFred::OperationContextCreator ctx;
    ::LibFred::DeleteDomainByFqdn{domain_fqdn}.exec(ctx);
    enable_auction(ctx, "cz");
    using namespace ::LibFred::Domain::Auction;
    BOOST_REQUIRE_EQUAL(GetAuctions{}.exec(ctx).size(), 0);
    const auto auction_id = CreateAuction{domain_fqdn}.exec(ctx);
    static constexpr auto external_auction_id = "my auction";
    UpdateAuction{auction_id}.set_external_auction_id(ExternalAuctionId{external_auction_id}).exec(ctx);
    FinishAuction{auction_id}.exec(ctx, FinishAuction::Release::later);
    const auto now = one_minute_before(ctx) + std::chrono::minutes{1};
    BOOST_CHECK_EQUAL(GetUnreleasedFqdn{}.exec(ctx, now - std::chrono::minutes{1}).size(), 0);
    BOOST_CHECK_EQUAL(GetUnreleasedFqdn{}.exec(ctx, now).size(), 0);
    BOOST_CHECK_EQUAL(GetUnreleasedFqdn{}.exec(ctx).size(), 1);
    BOOST_CHECK_EQUAL(GetUnreleasedFqdn{}.exec(ctx, now + std::chrono::minutes{1}).size(), 1);
}

BOOST_AUTO_TEST_SUITE_END()//TestDomainAuction
