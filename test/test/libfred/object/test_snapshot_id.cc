/*
 * Copyright (C) 2025  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <boost/test/unit_test.hpp>

#include "libfred/object_state/create_object_state_request_id.hh"
#include "libfred/object_state/perform_object_state_request.hh"
#include "libfred/registrable_object/snapshot_id.hh"
#include "libfred/registrable_object/contact/delete_contact.hh"
#include "libfred/registrable_object/contact/update_contact.hh"

#include "test/setup/fixtures.hh"

namespace {

struct HasRegistrar
{
    explicit HasRegistrar(::LibFred::OperationContext& ctx)
        : registrar{
                ctx,
                ::LibFred::CreateRegistrar{
                        "REG-COMMON",
                        "Common",
                        "Common ltd.",
                        {"No street"},
                        "Praha",
                        "CZ",
                        "reg@common.cz",
                        "https://common.cz",
                        "12344321"}}
    {}
    Test::Registrar registrar;
};

struct HasContact : HasRegistrar
{
    explicit HasContact(::LibFred::OperationContext& ctx)
        : HasRegistrar{ctx},
          contact{
                ctx,
                ::LibFred::CreateContact{
                        "TEST-CONTACT",
                        registrar.data.handle}},
          contact_create_snapshot_id{LibFred::RegistrableObject::get_snapshot_id(ctx)}
    {}
    Test::Contact contact;
    LibFred::RegistrableObject::SnapshotId contact_create_snapshot_id;
};

HasContact make_has_contact()
{
    LibFred::OperationContextCreator ctx;
    HasContact result{ctx};
    ctx.commit_transaction();
    return result;
}

struct HasContactFixture : Test::instantiate_db_template
{
    explicit HasContactFixture()
        : db{make_has_contact()}
    {}
    HasContact db;
};

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestSnapshotId)

BOOST_FIXTURE_TEST_CASE(contact_snapshot_happy, HasContactFixture)
{
    const auto start_snapshot_id = [this]()
    { // no data change, no state change
        LibFred::OperationContextCreator ctx;
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForUpdate{ctx}, db.contact_create_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        auto current_snapshot_id = LibFred::RegistrableObject::get_snapshot_id(ctx);
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, current_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForUpdate{ctx}, current_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        ctx.commit_transaction();
        return current_snapshot_id;
    }();
    { // data change, no state change
        LibFred::OperationContextCreator ctx;
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        LibFred::UpdateContactByHandle{db.contact.data.handle, db.registrar.data.handle}.set_email("tester@test.org").exec(ctx);
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id);
            BOOST_CHECK(result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, start_snapshot_id);
            BOOST_CHECK(result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        const auto current_snapshot_id = LibFred::RegistrableObject::get_snapshot_id(ctx);
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, current_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
    }
    const auto state_change_snapshot_id = [this, &start_snapshot_id]()
    { // no data change, state change
        LibFred::OperationContextCreator ctx;
        LibFred::CreateObjectStateRequestId{db.contact.data.id, {"contactInManualVerification"}}.exec(ctx);
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, start_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        auto current_snapshot_id = LibFred::RegistrableObject::get_snapshot_id(ctx);
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, current_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        LibFred::PerformObjectStateRequest{db.contact.data.id}.exec(ctx);
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(result.state_has_been_changed);
        }
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, start_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(result.state_has_been_changed);
        }
        {
            const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, current_snapshot_id);
            BOOST_CHECK(!result.data_has_been_changed);
            BOOST_CHECK(!result.state_has_been_changed);
        }
        ctx.commit_transaction();
        return current_snapshot_id;
    }();
    LibFred::OperationContextCreator ctx;
    {
        const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id);
        BOOST_CHECK(!result.data_has_been_changed);
        BOOST_CHECK(result.state_has_been_changed);
    }
    LibFred::UpdateContactByHandle{db.contact.data.handle, db.registrar.data.handle}.set_email("tester@test.org").exec(ctx);
    {
        const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id);
        BOOST_CHECK(result.data_has_been_changed);
        BOOST_CHECK(result.state_has_been_changed);
    }
    {
        const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, start_snapshot_id);
        BOOST_CHECK(result.data_has_been_changed);
        BOOST_CHECK(result.state_has_been_changed);
    }
    {
        const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, state_change_snapshot_id);
        BOOST_CHECK(result.data_has_been_changed);
        BOOST_CHECK(!result.state_has_been_changed);
    }
    const auto current_snapshot_id = LibFred::RegistrableObject::get_snapshot_id(ctx);
    {
        const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, current_snapshot_id);
        BOOST_CHECK(!result.data_has_been_changed);
        BOOST_CHECK(!result.state_has_been_changed);
    }
}

BOOST_FIXTURE_TEST_CASE(invalid_snapshot_id, HasContactFixture)
{
    LibFred::OperationContextCreator ctx;
    static const auto invalid_snapshot_id = Util::make_strong<LibFred::RegistrableObject::SnapshotId>(std::string{"invalid snapshot id"});
    BOOST_CHECK_EXCEPTION(
            LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, invalid_snapshot_id),
            LibFred::RegistrableObject::InvalidSnapshotId,
            [](auto&&) { return true; });
}

BOOST_FIXTURE_TEST_CASE(object_id_does_not_exist, HasContactFixture)
{
    LibFred::OperationContextCreator ctx;
    static const auto non_existent_contact_uuid = Util::make_strong<LibFred::RegistrableObject::Contact::ContactUuid>(boost::uuids::uuid{});
    BOOST_CHECK_EXCEPTION(
            LibFred::RegistrableObject::CheckObjectChange{non_existent_contact_uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id),
            LibFred::UnknownObjectId,
            [](auto&&) { return true; });
    const auto result = LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id);
    BOOST_CHECK(!result.data_has_been_changed);
    BOOST_CHECK(!result.state_has_been_changed);
    BOOST_CHECK_NO_THROW(LibFred::DeleteContactById{db.contact.data.id}.exec(ctx));
    BOOST_CHECK_EXCEPTION(
            LibFred::RegistrableObject::CheckObjectChange{db.contact.data.uuid}.exec(LibFred::OperationContextLockingForShare{ctx}, db.contact_create_snapshot_id),
            LibFred::UnknownObjectId,
            [](auto&&) { return true; });
}

BOOST_AUTO_TEST_SUITE_END()//TestSnapshotId
