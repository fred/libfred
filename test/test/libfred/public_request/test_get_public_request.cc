/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <boost/test/unit_test.hpp>

#include "libfred/public_request/create_public_request.hh"
#include "libfred/public_request/get_public_request.hh"
#include "libfred/public_request/get_public_request_types.hh"

#include "test/setup/fixtures.hh"

#include <algorithm>
#include <map>
#include <vector>

namespace {

struct HasOperationContext
    : Test::instantiate_db_template,
      Test::HasAutoRollbackingOperationContext
{
    explicit HasOperationContext()
        : Test::instantiate_db_template{},
          Test::HasAutoRollbackingOperationContext{}
    { }
};

struct HasAllObjects : HasOperationContext
{
    explicit HasAllObjects()
        : HasOperationContext{},
          registrar{ctx, Test::Setter::registrar(LibFred::CreateRegistrar{
                "REG-PUB-REQUEST",
                "-", "-", {"-"}, "-", "-", "-", "-", "-", "-"}, 2)},
          contact{ctx, Test::Setter::contact(LibFred::CreateContact{
                "CONTACT-PUB-REQUEST",
                registrar.data.handle})},
          domain{ctx, Test::Setter::domain(LibFred::CreateDomain{
                "domain-pub-request.cz",
                registrar.data.handle,
                contact.data.handle})},
          keyset{ctx, Test::Setter::keyset(LibFred::CreateKeyset{
                "KEYSET-PUB-REQUEST",
                registrar.data.handle})},
          nsset{ctx, Test::Setter::nsset(LibFred::CreateNsset{
                "NSSET-PUB-REQUEST",
                registrar.data.handle})}
    { }
    Test::Registrar registrar;
    Test::Contact contact;
    Test::Domain domain;
    Test::Keyset keyset;
    Test::Nsset nsset;
};

struct HasPublicRequest : HasAllObjects
{
    explicit HasPublicRequest()
        : types{LibFred::PublicRequest::GetPublicRequestTypes{}.exec(ctx)},
          public_request{
                {
                    LibFred::Object_Type::contact,
                    Test::PublicRequest{
                        ctx,
                        *begin(types),
                        contact.data.id,
                        registrar,
                        "email@answer.to",
                        LibFred::PublicRequest::Status::opened,
                        LibFred::PublicRequest::OnStatusAction::scheduled,
                        std::chrono::seconds{-3600},
                        42,
                        std::chrono::seconds{-1800},
                        142}
                },
                {
                    LibFred::Object_Type::domain,
                    Test::PublicRequest{
                        ctx,
                        *begin(types),
                        domain.data.id,
                        registrar,
                        "email@answer.to",
                        LibFred::PublicRequest::Status::opened,
                        LibFred::PublicRequest::OnStatusAction::scheduled,
                        std::chrono::seconds{-3600},
                        42,
                        std::chrono::seconds{-1800},
                        142}
                },
                {
                    LibFred::Object_Type::keyset,
                    Test::PublicRequest{
                        ctx,
                        *begin(types),
                        keyset.data.id,
                        registrar,
                        "email@answer.to",
                        LibFred::PublicRequest::Status::opened,
                        LibFred::PublicRequest::OnStatusAction::scheduled,
                        std::chrono::seconds{-3600},
                        42,
                        std::chrono::seconds{-1800},
                        142}
                },
                {
                    LibFred::Object_Type::nsset,
                    Test::PublicRequest{
                        ctx,
                        *begin(types),
                        nsset.data.id,
                        registrar,
                        "email@answer.to",
                        LibFred::PublicRequest::Status::opened,
                        LibFred::PublicRequest::OnStatusAction::scheduled,
                        std::chrono::seconds{-3600},
                        42,
                        std::chrono::seconds{-1800},
                        142}
                }
          },
          max_public_request_id{std::max({public_request.find(LibFred::Object_Type::contact)->second.data.id,
                                          public_request.find(LibFred::Object_Type::domain)->second.data.id,
                                          public_request.find(LibFred::Object_Type::keyset)->second.data.id,
                                          public_request.find(LibFred::Object_Type::nsset)->second.data.id})}
    { }
    LibFred::PublicRequest::PublicRequestTypes types;
    std::map<LibFred::Object_Type::Enum, Test::PublicRequest> public_request;
    unsigned long long max_public_request_id;
};

static constexpr auto statuses = {
        LibFred::PublicRequest::Status::opened,
        LibFred::PublicRequest::Status::resolved,
        LibFred::PublicRequest::Status::invalidated
};

static constexpr auto actions = {
        LibFred::PublicRequest::OnStatusAction::scheduled,
        LibFred::PublicRequest::OnStatusAction::processed,
        LibFred::PublicRequest::OnStatusAction::failed
};

bool is_closed(LibFred::PublicRequest::Status::Enum status)
{
    switch (status)
    {
        case LibFred::PublicRequest::Status::opened:
            return false;
        case LibFred::PublicRequest::Status::resolved:
            return true;
        case LibFred::PublicRequest::Status::invalidated:
            return true;
    }
    return true;
}

struct HasAllPublicRequests : HasAllObjects
{
    explicit HasAllPublicRequests()
        : types{LibFred::PublicRequest::GetPublicRequestTypes{}.exec(ctx)}
    {
        auto create_time_shift = std::chrono::seconds{0};
        auto logger_id = 1ull;
        const auto objects = {contact.data.id, domain.data.id, keyset.data.id, nsset.data.id};
        std::for_each(begin(types), end(types), [&](auto&& type)
        {
            std::for_each(begin(statuses), end(statuses), [&](auto&& status)
            {
                std::for_each(begin(actions), end(actions), [&](auto&& action)
                {
                    if (is_closed(status))
                    {
                        std::for_each(begin(objects), end(objects), [&](auto&& object_id)
                        {
                            public_requests.emplace_back(
                                    ctx,
                                    type,
                                    object_id,
                                    registrar,
                                    "email@answer.to",
                                    status,
                                    action,
                                    create_time_shift - std::chrono::seconds{3600},
                                    logger_id,
                                    create_time_shift - std::chrono::seconds{1800},
                                    logger_id + 1);
                        });
                        logger_id += 2;
                    }
                    else
                    {
                        std::for_each(begin(objects), end(objects), [&](auto&& object_id)
                        {
                            public_requests.emplace_back(
                                    ctx,
                                    type,
                                    object_id,
                                    registrar,
                                    "email@answer.to",
                                    status,
                                    action,
                                    create_time_shift - std::chrono::seconds{3600},
                                    logger_id,
                                    boost::none,
                                    boost::none);
                        });
                        logger_id += 1;
                    }
                    create_time_shift -= std::chrono::seconds{3600};
                });
            });
        });
    }
    LibFred::PublicRequest::PublicRequestTypes types;
    std::vector<Test::PublicRequest> public_requests;
};

struct ObjectData : boost::static_visitor<void>
{
    explicit ObjectData(const Test::Contact& contact)
        : id{contact.data.id},
          uuid{get_raw_value_from(contact.data.uuid)},
          string_id{contact.data.handle},
          type{LibFred::Object_Type::contact}
    { }
    explicit ObjectData(const Test::Domain& domain)
        : id{domain.data.id},
          uuid{get_raw_value_from(domain.data.uuid)},
          string_id{domain.data.fqdn},
          type{LibFred::Object_Type::domain}
    { }
    explicit ObjectData(const Test::Keyset& keyset)
        : id{keyset.data.id},
          uuid{get_raw_value_from(keyset.data.uuid)},
          string_id{keyset.data.handle},
          type{LibFred::Object_Type::keyset}
    { }
    explicit ObjectData(const Test::Nsset& nsset)
        : id{nsset.data.id},
          uuid{get_raw_value_from(nsset.data.uuid)},
          string_id{nsset.data.handle},
          type{LibFred::Object_Type::nsset}
    { }
    void operator()(const LibFred::PublicRequest::PublicRequestData::ContactDataLight& data) const
    {
        BOOST_CHECK_EQUAL(id, data.id);
        BOOST_CHECK_EQUAL(uuid, data.uuid);
        BOOST_CHECK_EQUAL(string_id, data.handle);
        BOOST_CHECK_EQUAL(type, LibFred::Object_Type::contact);
    }
    void operator()(const LibFred::PublicRequest::PublicRequestData::DomainDataLight& data) const
    {
        BOOST_CHECK_EQUAL(id, data.id);
        BOOST_CHECK_EQUAL(uuid, data.uuid);
        BOOST_CHECK_EQUAL(string_id, data.fqdn);
        BOOST_CHECK_EQUAL(type, LibFred::Object_Type::domain);
    }
    void operator()(const LibFred::PublicRequest::PublicRequestData::KeysetDataLight& data) const
    {
        BOOST_CHECK_EQUAL(id, data.id);
        BOOST_CHECK_EQUAL(uuid, data.uuid);
        BOOST_CHECK_EQUAL(string_id, data.handle);
        BOOST_CHECK_EQUAL(type, LibFred::Object_Type::keyset);
    }
    void operator()(const LibFred::PublicRequest::PublicRequestData::NssetDataLight& data) const
    {
        BOOST_CHECK_EQUAL(id, data.id);
        BOOST_CHECK_EQUAL(uuid, data.uuid);
        BOOST_CHECK_EQUAL(string_id, data.handle);
        BOOST_CHECK_EQUAL(type, LibFred::Object_Type::nsset);
    }
    std::vector<LibFred::PublicRequest::PublicRequestData> get_public_requests_by_id(
            const LibFred::OperationContext& ctx,
            LibFred::PublicRequest::Filter<std::string> type = LibFred::PublicRequest::any_type(),
            LibFred::PublicRequest::Filter<LibFred::PublicRequest::Status::Enum> status = LibFred::PublicRequest::any_status(),
            LibFred::PublicRequest::Filter<LibFred::PublicRequest::OnStatusAction::Enum> on_status_action = LibFred::PublicRequest::any_on_status_action(),
            LibFred::PublicRequest::ChronologicOrder order = LibFred::PublicRequest::ChronologicOrder::from_newest) const
    {
        switch (this->type)
        {
            case LibFred::Object_Type::contact:
                return LibFred::PublicRequest::GetPublicRequestsOfContact{id, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::domain:
                return LibFred::PublicRequest::GetPublicRequestsOfDomain{id, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::keyset:
                return LibFred::PublicRequest::GetPublicRequestsOfKeyset{id, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::nsset:
                return LibFred::PublicRequest::GetPublicRequestsOfNsset{id, status, on_status_action, type, order}.exec(ctx);
        }
        throw std::runtime_error{"unexpected object type"};
    }
    std::vector<LibFred::PublicRequest::PublicRequestData> get_public_requests_by_uuid(
            const LibFred::OperationContext& ctx,
            LibFred::PublicRequest::Filter<std::string> type = LibFred::PublicRequest::any_type(),
            LibFred::PublicRequest::Filter<LibFred::PublicRequest::Status::Enum> status = LibFred::PublicRequest::any_status(),
            LibFred::PublicRequest::Filter<LibFred::PublicRequest::OnStatusAction::Enum> on_status_action = LibFred::PublicRequest::any_on_status_action(),
            LibFred::PublicRequest::ChronologicOrder order = LibFred::PublicRequest::ChronologicOrder::from_newest) const
    {
        switch (this->type)
        {
            case LibFred::Object_Type::contact:
                return LibFred::PublicRequest::GetPublicRequestsOfContact{uuid, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::domain:
                return LibFred::PublicRequest::GetPublicRequestsOfDomain{uuid, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::keyset:
                return LibFred::PublicRequest::GetPublicRequestsOfKeyset{uuid, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::nsset:
                return LibFred::PublicRequest::GetPublicRequestsOfNsset{uuid, status, on_status_action, type, order}.exec(ctx);
        }
        throw std::runtime_error{"unexpected object type"};
    }
    std::vector<LibFred::PublicRequest::PublicRequestData> get_public_requests_by_handle(
            const LibFred::OperationContext& ctx,
            LibFred::PublicRequest::Filter<std::string> type = LibFred::PublicRequest::any_type(),
            LibFred::PublicRequest::Filter<LibFred::PublicRequest::Status::Enum> status = LibFred::PublicRequest::any_status(),
            LibFred::PublicRequest::Filter<LibFred::PublicRequest::OnStatusAction::Enum> on_status_action = LibFred::PublicRequest::any_on_status_action(),
            LibFred::PublicRequest::ChronologicOrder order = LibFred::PublicRequest::ChronologicOrder::from_newest) const
    {
        switch (this->type)
        {
            case LibFred::Object_Type::contact:
                return LibFred::PublicRequest::GetPublicRequestsOfContact{string_id, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::domain:
                return LibFred::PublicRequest::GetPublicRequestsOfDomain{string_id, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::keyset:
                return LibFred::PublicRequest::GetPublicRequestsOfKeyset{string_id, status, on_status_action, type, order}.exec(ctx);
            case LibFred::Object_Type::nsset:
                return LibFred::PublicRequest::GetPublicRequestsOfNsset{string_id, status, on_status_action, type, order}.exec(ctx);
        }
        throw std::runtime_error{"unexpected object type"};
    }

    unsigned long long id;
    const boost::uuids::uuid& uuid;
    const std::string& string_id;
    LibFred::Object_Type::Enum type;
};

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestPublicRequest)
BOOST_AUTO_TEST_SUITE(GetPublicRequest)

BOOST_FIXTURE_TEST_CASE(get_public_request_types, HasOperationContext)
{
    const auto types = LibFred::PublicRequest::GetPublicRequestTypes{}.exec(ctx);
    BOOST_CHECK_LT(0, types.size());
}

BOOST_FIXTURE_TEST_CASE(get_public_request, HasPublicRequest,
                        *boost::unit_test::depends_on("TestPublicRequest/GetPublicRequest/get_public_request_types"))
{
    const auto objects = { ObjectData{contact}, ObjectData{domain}, ObjectData{keyset}, ObjectData{nsset} };
    std::for_each(begin(objects), end(objects), [&](auto&& object)
    {
        const auto& public_request_data = public_request.find(object.type)->second.data;
        BOOST_CHECK_EQUAL(public_request_data.type, *begin(types));
        BOOST_CHECK_EQUAL(public_request_data.status, LibFred::PublicRequest::Status::opened);
        BOOST_CHECK_EQUAL(public_request_data.on_status_action, LibFred::PublicRequest::OnStatusAction::scheduled);
        BOOST_REQUIRE(public_request_data.registrar_data != boost::none);
        BOOST_CHECK_EQUAL(public_request_data.registrar_data->id, registrar.data.id);
        BOOST_CHECK_EQUAL(public_request_data.registrar_data->handle, registrar.data.handle);
        boost::apply_visitor(object, public_request_data.object_data);
        BOOST_CHECK_EQUAL(public_request_data.email_to_answer, "email@answer.to");
        BOOST_CHECK_EQUAL(public_request_data.create_request_id, "42");
        BOOST_REQUIRE(public_request_data.resolve_time != boost::none);
        BOOST_CHECK_EQUAL(std::chrono::duration_cast<std::chrono::seconds>(*public_request_data.resolve_time - public_request_data.create_time).count(), 1800);
        BOOST_CHECK_EQUAL(public_request_data.resolve_request_id, "142");
    });
    BOOST_CHECK_EXCEPTION(LibFred::PublicRequest::GetPublicRequest{max_public_request_id + 1}.exec(ctx),
                          LibFred::PublicRequest::PublicRequestDoesNotExist,
                          [](const auto&) { return true; });
}

BOOST_FIXTURE_TEST_CASE(get_public_requests, HasAllPublicRequests,
                        *boost::unit_test::depends_on("TestPublicRequest/GetPublicRequest/get_public_request"))
{
    const auto objects = { ObjectData{contact}, ObjectData{domain}, ObjectData{keyset}, ObjectData{nsset} };
    std::for_each(begin(objects), end(objects), [&](auto&& object)
    {
        BOOST_REQUIRE_EQUAL(public_requests.size(), objects.size() * types.size() * statuses.size() * actions.size());
        {
            const auto result = object.get_public_requests_by_id(ctx);
            BOOST_CHECK_EQUAL(objects.size() * result.size(), public_requests.size());
            auto public_request_iter = begin(result);
            auto logger_id = 1ull;
            std::for_each(begin(types), end(types), [&](auto&& type)
            {
                std::for_each(begin(statuses), end(statuses), [&](auto&& status)
                {
                    std::for_each(begin(actions), end(actions), [&](auto&& action)
                    {
                        BOOST_REQUIRE(public_request_iter != end(result));
                        BOOST_TEST_MESSAGE(public_request_iter->id << ": " << type << " " << Conversion::Enums::to_db_handle(status) << " " << Conversion::Enums::to_db_handle(action));
                        BOOST_CHECK_EQUAL(public_request_iter->type, type);
                        BOOST_CHECK_EQUAL(public_request_iter->status, status);
                        BOOST_CHECK_EQUAL(public_request_iter->on_status_action, action);
                        BOOST_REQUIRE(public_request_iter->registrar_data != boost::none);
                        BOOST_CHECK_EQUAL(public_request_iter->registrar_data->id, registrar.data.id);
                        BOOST_CHECK_EQUAL(public_request_iter->registrar_data->handle, registrar.data.handle);
                        boost::apply_visitor(object, public_request_iter->object_data);
                        BOOST_CHECK_EQUAL(public_request_iter->email_to_answer, "email@answer.to");
                        BOOST_CHECK_EQUAL(public_request_iter->create_request_id, std::to_string(logger_id));
                        if (is_closed(status))
                        {
                            BOOST_REQUIRE(public_request_iter->resolve_time != boost::none);
                            BOOST_CHECK_EQUAL(std::chrono::duration_cast<std::chrono::seconds>(*public_request_iter->resolve_time - public_request_iter->create_time).count(), 1800);
                            BOOST_CHECK_EQUAL(public_request_iter->resolve_request_id, std::to_string(logger_id + 1));
                            logger_id += 2;
                        }
                        else
                        {
                            BOOST_CHECK(public_request_iter->resolve_time == boost::none);
                            BOOST_CHECK_EQUAL(public_request_iter->resolve_request_id, "");
                            logger_id += 1;
                        }
                        ++public_request_iter;
                    });
                });
            });
            BOOST_CHECK(public_request_iter == end(result));
        }
        {
            const auto result = object.get_public_requests_by_id(
                    ctx,
                    LibFred::PublicRequest::Filter<std::string>{{types}},
                    LibFred::PublicRequest::Filter<LibFred::PublicRequest::Status::Enum>{{statuses}},
                    LibFred::PublicRequest::Filter<LibFred::PublicRequest::OnStatusAction::Enum>{{actions}},
                    LibFred::PublicRequest::ChronologicOrder::from_oldest);
            BOOST_CHECK_EQUAL(objects.size() * result.size(), public_requests.size());
            auto public_request_iter = rbegin(result);
            auto logger_id = 1ull;
            std::for_each(begin(types), end(types), [&](auto&& type)
            {
                std::for_each(begin(statuses), end(statuses), [&](auto&& status)
                {
                    std::for_each(begin(actions), end(actions), [&](auto&& action)
                    {
                        BOOST_REQUIRE(public_request_iter != rend(result));
                        BOOST_TEST_MESSAGE(public_request_iter->id << ": " << type << " " << Conversion::Enums::to_db_handle(status) << " " << Conversion::Enums::to_db_handle(action));
                        BOOST_CHECK_EQUAL(public_request_iter->type, type);
                        BOOST_CHECK_EQUAL(public_request_iter->status, status);
                        BOOST_CHECK_EQUAL(public_request_iter->on_status_action, action);
                        BOOST_REQUIRE(public_request_iter->registrar_data != boost::none);
                        BOOST_CHECK_EQUAL(public_request_iter->registrar_data->id, registrar.data.id);
                        BOOST_CHECK_EQUAL(public_request_iter->registrar_data->handle, registrar.data.handle);
                        boost::apply_visitor(object, public_request_iter->object_data);
                        BOOST_CHECK_EQUAL(public_request_iter->email_to_answer, "email@answer.to");
                        BOOST_CHECK_EQUAL(public_request_iter->create_request_id, std::to_string(logger_id));
                        if (is_closed(status))
                        {
                            BOOST_REQUIRE(public_request_iter->resolve_time != boost::none);
                            BOOST_CHECK_EQUAL(std::chrono::duration_cast<std::chrono::seconds>(*public_request_iter->resolve_time - public_request_iter->create_time).count(), 1800);
                            BOOST_CHECK_EQUAL(public_request_iter->resolve_request_id, std::to_string(logger_id + 1));
                            logger_id += 2;
                        }
                        else
                        {
                            BOOST_CHECK(public_request_iter->resolve_time == boost::none);
                            BOOST_CHECK_EQUAL(public_request_iter->resolve_request_id, "");
                            logger_id += 1;
                        }
                        ++public_request_iter;
                    });
                });
            });
            BOOST_CHECK(public_request_iter == rend(result));
        }
        std::for_each(begin(types), end(types), [&](auto&& type)
        {
            {
                const auto result = object.get_public_requests_by_id(ctx, {{type}});
                BOOST_CHECK_EQUAL(result.size(), statuses.size() * actions.size());
                std::for_each(begin(result), end(result), [&](auto&& public_request)
                {
                    BOOST_CHECK_EQUAL(public_request.type, type);
                });
            }
            std::for_each(begin(statuses), end(statuses), [&](auto&& status)
            {
                {
                    const auto result = object.get_public_requests_by_uuid(ctx, {{type}}, {{status}});
                    BOOST_CHECK_EQUAL(result.size(), actions.size());
                    std::for_each(begin(result), end(result), [&](auto&& public_request)
                    {
                        BOOST_CHECK_EQUAL(public_request.type, type);
                        BOOST_CHECK_EQUAL(public_request.status, status);
                    });
                }
                std::for_each(begin(actions), end(actions), [&](auto&& action)
                {
                    {
                        const auto result = object.get_public_requests_by_handle(ctx, {{type}}, {{status}}, {{action}});
                        BOOST_CHECK_EQUAL(result.size(), 1);
                        std::for_each(begin(result), end(result), [&](auto&& public_request)
                        {
                            BOOST_CHECK_EQUAL(public_request.type, type);
                            BOOST_CHECK_EQUAL(public_request.status, status);
                            BOOST_CHECK_EQUAL(public_request.on_status_action, action);
                        });
                    }
                });
            });
        });
    });

    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfContact{
            contact.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 1);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfDomain{
            contact.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfKeyset{
            contact.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfNsset{
            contact.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);

    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfContact{
            domain.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfDomain{
            domain.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 1);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfKeyset{
            domain.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfNsset{
            domain.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);

    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfContact{
            keyset.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfDomain{
            keyset.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfKeyset{
            keyset.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 1);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfNsset{
            keyset.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);

    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfContact{
            nsset.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfDomain{
            nsset.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfKeyset{
            nsset.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 0);
    BOOST_CHECK_EQUAL((LibFred::PublicRequest::GetPublicRequestsOfNsset{
            nsset.data.id,
            {{LibFred::PublicRequest::Status::opened}},
            {{LibFred::PublicRequest::OnStatusAction::scheduled}},
            {{*begin(types)}}}.exec(ctx).size()), 1);
}

BOOST_AUTO_TEST_SUITE_END()//TestPublicRequest/GetPublicRequest
BOOST_AUTO_TEST_SUITE_END()//TestPublicRequest
