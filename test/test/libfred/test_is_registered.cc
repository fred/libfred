/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/object/object_type.hh"
#include "src/libfred/opcontext.hh"
#include "src/libfred/registrable_object/contact/create_contact.hh"
#include "src/libfred/registrable_object/domain/create_domain.hh"
#include "src/libfred/registrable_object/is_registered.hh"
#include "src/libfred/registrable_object/keyset/create_keyset.hh"
#include "src/libfred/registrable_object/nsset/create_nsset.hh"

#include "test/setup/fixtures.hh"

#include <boost/test/unit_test.hpp>
#include <boost/uuid/uuid.hpp>

#include <string>

const std::string server_name = "test-is-registered";

namespace {

struct HasOperationContext
    : Test::instantiate_db_template,
      Test::HasAutoRollbackingOperationContext
{
    explicit HasOperationContext()
        : Test::instantiate_db_template{},
          Test::HasAutoRollbackingOperationContext{}
    { }
};

struct HasAllObjects : HasOperationContext
{
    explicit HasAllObjects()
        : HasOperationContext{},
          registrar{ctx, Test::Setter::registrar(LibFred::CreateRegistrar{
                "TEST-REGISTRAR",
                "-", "-", {"-"}, "-", "-", "-", "-", "-", "-"}, 2)},
          contact{ctx, Test::Setter::contact(LibFred::CreateContact{
                "TEST-CONTACT",
                registrar.data.handle})},
          domain{ctx, Test::Setter::domain(LibFred::CreateDomain{
                "test-domain.cz",
                registrar.data.handle,
                contact.data.handle})},
          keyset{ctx, Test::Setter::keyset(LibFred::CreateKeyset{
                "TEST-KEYSET",
                registrar.data.handle})},
          nsset{ctx, Test::Setter::nsset(LibFred::CreateNsset{
                "TEST-NSSET",
                registrar.data.handle})}
    { }
    Test::Registrar registrar;
    Test::Contact contact;
    Test::Domain domain;
    Test::Keyset keyset;
    Test::Nsset nsset;
};

struct IsRegisteredFixture: HasAllObjects
{
    boost::uuids::uuid nonexistent_uuid;

    IsRegisteredFixture()
        : nonexistent_uuid(boost::uuids::string_generator{}("771a18e4-ce0e-4345-9ae5-6ca9fcf72048"))
    {
    }
    ~IsRegisteredFixture()
    {
    }
};

}

BOOST_FIXTURE_TEST_SUITE(TestIsRegistered, IsRegisteredFixture)

BOOST_AUTO_TEST_CASE(check_is_registered)
{
    BOOST_CHECK(!::LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::contact, nonexistent_uuid));
    BOOST_CHECK(!::LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::domain, nonexistent_uuid));
    BOOST_CHECK(!::LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::nsset, nonexistent_uuid));
    BOOST_CHECK(!::LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::keyset, nonexistent_uuid));

    BOOST_CHECK(!::LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::contact, nonexistent_uuid));
    BOOST_CHECK(!::LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::domain, nonexistent_uuid));
    BOOST_CHECK(!::LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::nsset, nonexistent_uuid));
    BOOST_CHECK(!::LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::keyset, nonexistent_uuid));

    BOOST_CHECK(::LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::contact, get_raw_value_from(contact.data.uuid)));
    BOOST_CHECK(::LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::domain, get_raw_value_from(domain.data.uuid)));
    BOOST_CHECK(::LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::nsset, get_raw_value_from(nsset.data.uuid)));
    BOOST_CHECK(::LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::keyset, get_raw_value_from(keyset.data.uuid)));

    BOOST_CHECK(::LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::contact, get_raw_value_from(contact.data.history_uuid)));
    BOOST_CHECK(::LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::domain, get_raw_value_from(domain.data.history_uuid)));
    BOOST_CHECK(::LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::nsset, get_raw_value_from(nsset.data.history_uuid)));
    BOOST_CHECK(::LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::keyset, get_raw_value_from(keyset.data.history_uuid)));
};

BOOST_AUTO_TEST_SUITE_END();
