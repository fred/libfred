/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/libfred/object/object_type.hh"
#include "src/libfred/object_state/create_object_state_request_id.hh"
#include "src/libfred/object_state/perform_object_state_request.hh"
#include "src/libfred/object_state/cancel_object_state_request_id.hh"
#include "src/libfred/opcontext.hh"
#include "src/libfred/registrable_object/contact/create_contact.hh"
#include "src/libfred/registrable_object/contact/get_contact_state_history.hh"
#include "src/libfred/registrable_object/domain/create_domain.hh"
#include "src/libfred/registrable_object/domain/get_domain_state_history.hh"
#include "src/libfred/registrable_object/keyset/create_keyset.hh"
#include "src/libfred/registrable_object/keyset/get_keyset_state_history.hh"
#include "src/libfred/registrable_object/nsset/create_nsset.hh"
#include "src/libfred/registrable_object/nsset/get_nsset_state_history.hh"

#include "test/setup/fixtures.hh"

#include <boost/test/unit_test.hpp>
#include <boost/uuid/string_generator.hpp>

namespace {

struct HasOperationContext
    : Test::instantiate_db_template,
      Test::HasAutoRollbackingOperationContext
{
    explicit HasOperationContext()
        : Test::instantiate_db_template{},
          Test::HasAutoRollbackingOperationContext{}
    { }
};

struct HasRegistrar : HasOperationContext
{
    explicit HasRegistrar()
        : HasOperationContext{},
          registrar{ctx, Test::Setter::registrar(LibFred::CreateRegistrar{
                "TEST-REGISTRAR",
                "-", "-", {"-"}, "-", "-", "-", "-", "-", "-"}, 2)}
    { }
    Test::Registrar registrar;
};

struct HasContact : HasRegistrar
{
    explicit HasContact()
        : HasRegistrar{},
          contact{ctx, Test::Setter::contact(LibFred::CreateContact{
                "TEST-CONTACT",
                registrar.data.handle})}
    { }
    Test::Contact contact;
};

struct HasDomain : HasContact
{
    explicit HasDomain()
        : HasContact{},
          domain{ctx, Test::Setter::domain(LibFred::CreateDomain{
                "test-domain.cz",
                registrar.data.handle,
                contact.data.handle})}
    { }
    Test::Domain domain;
};

struct HasKeyset : HasRegistrar
{
    explicit HasKeyset()
        : HasRegistrar{},
          keyset{ctx, Test::Setter::keyset(LibFred::CreateKeyset{
                "TEST-KEYSET",
                registrar.data.handle})}
    { }
    Test::Keyset keyset;
};

struct HasNsset : HasRegistrar
{
    explicit HasNsset()
        : HasRegistrar{},
          nsset{ctx, Test::Setter::nsset(LibFred::CreateNsset{
                "TEST-NSSET",
                registrar.data.handle})}
    { }
    Test::Nsset nsset;
};

struct HasNonexistentHandle : HasOperationContext
{
    std::string nonexistent_handle = "NONEXISTENT-HANDLE";
};

struct HasNonexistentFqdn : HasOperationContext
{
    std::string nonexistent_fqdn = "nonexistent-fqdn.cz";
};

struct HasNonexistentId : HasOperationContext
{
    unsigned long long nonexistent_id = 0;
};

struct HasNonexistentUuid : HasOperationContext
{
    boost::uuids::uuid nonexistent_uuid =
            boost::uuids::string_generator{}("771a18e4-ce0e-4345-9ae5-6ca9fcf72048");
};

template <typename Op>
void perform_object_state_request_operation(
            const LibFred::OperationContext& ctx,
            const LibFred::InfoContactData& contact,
            const LibFred::InfoDomainData& domain,
            const LibFred::InfoKeysetData& keyset,
            const LibFred::InfoNssetData& nsset)
{
    Op{contact.id, {LibFred::RegistrableObject::Contact::IdentifiedContact::name}}.exec(ctx);
    Op{domain.id, {LibFred::RegistrableObject::Domain::ServerInzoneManual::name}}.exec(ctx);
    Op{keyset.id, {LibFred::RegistrableObject::Keyset::ServerDeleteProhibited::name}}.exec(ctx);
    Op{nsset.id, {LibFred::RegistrableObject::Nsset::ServerTransferProhibited::name}}.exec(ctx);
    LibFred::PerformObjectStateRequest{0}.exec(ctx);
}

void create_object_state_request(
            const LibFred::OperationContext& ctx,
            const LibFred::InfoContactData& contact,
            const LibFred::InfoDomainData& domain,
            const LibFred::InfoKeysetData& keyset,
            const LibFred::InfoNssetData& nsset)
{
    perform_object_state_request_operation<LibFred::CreateObjectStateRequestId>(
            ctx,
            contact,
            domain,
            keyset,
            nsset);
}

void cancel_object_state_request(
        const LibFred::OperationContext& ctx,
        const LibFred::InfoContactData& contact,
        const LibFred::InfoDomainData& domain,
        const LibFred::InfoKeysetData& keyset,
        const LibFred::InfoNssetData& nsset)
{
    perform_object_state_request_operation<LibFred::CancelObjectStateRequestId>(
            ctx,
            contact,
            domain,
            keyset,
            nsset);
}

struct SetStateUp
{
    explicit SetStateUp(
            const LibFred::InfoContactData& contact,
            const LibFred::InfoDomainData& domain,
            const LibFred::InfoKeysetData& keyset,
            const LibFred::InfoNssetData& nsset)
    {
        LibFred::OperationContextCreator ctx;
        create_object_state_request(ctx, contact, domain, keyset, nsset);
        ctx.commit_transaction();
    }
};

struct SetStateDown
{
    explicit SetStateDown(
            const LibFred::InfoContactData& contact,
            const LibFred::InfoDomainData& domain,
            const LibFred::InfoKeysetData& keyset,
            const LibFred::InfoNssetData& nsset)
    {
        LibFred::OperationContextCreator ctx;
        cancel_object_state_request(ctx, contact, domain, keyset, nsset);
        ctx.commit_transaction();
    }
};

struct DiscontinuityUp
{
    explicit DiscontinuityUp(
            const LibFred::InfoContactData& contact,
            const LibFred::InfoDomainData& domain,
            const LibFred::InfoKeysetData& keyset,
            const LibFred::InfoNssetData& nsset)
    {
        LibFred::OperationContextCreator ctx;
        create_object_state_request(ctx, contact, domain, keyset, nsset);
        cancel_object_state_request(ctx, contact, domain, keyset, nsset);
        ctx.commit_transaction();
    }
};

struct DiscontinuityDown
{
    explicit DiscontinuityDown(
            const LibFred::InfoContactData& contact,
            const LibFred::InfoDomainData& domain,
            const LibFred::InfoKeysetData& keyset,
            const LibFred::InfoNssetData& nsset)
    {
        LibFred::OperationContextCreator ctx;
        cancel_object_state_request(ctx, contact, domain, keyset, nsset);
        create_object_state_request(ctx, contact, domain, keyset, nsset);
        ctx.commit_transaction();
    }
};

struct Commit
{
    explicit Commit(LibFred::OperationContextCreator& ctx)
    {
        ctx.commit_transaction();
    }
};

struct HasAllObjectsWithStateHistory : HasDomain
{
    explicit HasAllObjectsWithStateHistory()
        : HasDomain{},
          keyset{ctx, Test::Setter::keyset(LibFred::CreateKeyset{
                "TEST-KEYSET",
                registrar.data.handle})},
          nsset{ctx, Test::Setter::nsset(LibFred::CreateNsset{
                "TEST-NSSET",
                registrar.data.handle})},
          commit{ctx},
          state_0_break{contact.data, domain.data, keyset.data, nsset.data},
          state_1_begin{contact.data, domain.data, keyset.data, nsset.data},
          state_1_end{contact.data, domain.data, keyset.data, nsset.data},
          state_2_begin{contact.data, domain.data, keyset.data, nsset.data},
          state_2_break{contact.data, domain.data, keyset.data, nsset.data}
    { }
    Test::Keyset keyset;
    Test::Nsset nsset;
    Commit commit;
    DiscontinuityUp state_0_break;
    SetStateUp state_1_begin;
    SetStateDown state_1_end;
    SetStateUp state_2_begin;
    DiscontinuityDown state_2_break;
};

template <typename S>
bool operator==(
        const LibFred::RegistrableObject::StateHistory<S>& lhs,
        const LibFred::RegistrableObject::StateHistory<S>& rhs) noexcept
{
    if ((lhs.timeline.size() != rhs.timeline.size()) ||
        (lhs.valid_to != rhs.valid_to))
    {
        return false;
    }
    for (std::size_t idx = 0; idx < lhs.timeline.size(); ++idx)
    {
        if ((lhs.timeline[idx].valid_from != rhs.timeline[idx].valid_from) ||
            (lhs.timeline[idx].state != rhs.timeline[idx].state))
        {
            return false;
        }
    }
    return true;
}

const auto no_limit = LibFred::RegistrableObject::HistoryInterval::NoLimit{};
const auto no_lower_limit = LibFred::RegistrableObject::HistoryInterval::LowerLimit{no_limit};
const auto no_upper_limit = LibFred::RegistrableObject::HistoryInterval::UpperLimit{no_limit};
const auto unlimited = LibFred::RegistrableObject::HistoryInterval{no_lower_limit, no_upper_limit};

template <typename T, typename ...Flags>
struct FlagsToString
{
    template <typename F, int idx>
    Util::FlagSetVisiting visit(const Util::FlagSet<T, Flags...>& fs)
    {
        if (fs.template is_set<F>())
        {
            if (str.empty())
            {
                str = F::name;
            }
            else
            {
                str.append(std::string{", "} + F::name);
            }
        }
        return Util::FlagSetVisiting::can_continue;
    }
    std::string str;
};

}//namespace {anonymous}

namespace Util {

template <typename T, typename ...Flags>
std::ostream& operator<<(std::ostream& out, const FlagSet<T, Flags...>& flags)
{
    auto visitor = FlagsToString<T, Flags...>{};
    flags.visit(visitor);
    return out << ("<" + visitor.str + ">");
}

}//namespace Util

BOOST_AUTO_TEST_SUITE(TestStateHistory)

BOOST_FIXTURE_TEST_CASE(contact_handle_does_not_exist, HasNonexistentHandle)
{
    using namespace LibFred::RegistrableObject::Contact;
    BOOST_CHECK_EXCEPTION(GetContactStateHistoryByHandle{nonexistent_handle}.exec(ctx, unlimited),
                          GetContactStateHistoryByHandle::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(contact_id_does_not_exist, HasNonexistentId)
{
    using namespace LibFred::RegistrableObject::Contact;
    BOOST_CHECK_EXCEPTION(GetContactStateHistoryById{nonexistent_id}.exec(ctx, unlimited),
                          GetContactStateHistoryById::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(contact_uuid_does_not_exist, HasNonexistentUuid)
{
    using namespace LibFred::RegistrableObject::Contact;
    const auto uuid = Util::make_strong<ContactUuid>(nonexistent_uuid);
    BOOST_CHECK_EXCEPTION(GetContactStateHistoryByUuid{uuid}.exec(ctx, unlimited),
                          GetContactStateHistoryByUuid::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(domain_fqdn_does_not_exist, HasNonexistentFqdn)
{
    using namespace LibFred::RegistrableObject::Domain;
    BOOST_CHECK_EXCEPTION(GetDomainStateHistoryByFqdn{nonexistent_fqdn}.exec(ctx, unlimited),
                          GetDomainStateHistoryByFqdn::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(domain_id_does_not_exist, HasNonexistentId)
{
    using namespace LibFred::RegistrableObject::Domain;
    BOOST_CHECK_EXCEPTION(GetDomainStateHistoryById{nonexistent_id}.exec(ctx, unlimited),
                          GetDomainStateHistoryById::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(domain_uuid_does_not_exist, HasNonexistentUuid)
{
    using namespace LibFred::RegistrableObject::Domain;
    const auto uuid = Util::make_strong<DomainUuid>(nonexistent_uuid);
    BOOST_CHECK_EXCEPTION(GetDomainStateHistoryByUuid{uuid}.exec(ctx, unlimited),
                          GetDomainStateHistoryByUuid::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(keyset_handle_does_not_exist, HasNonexistentHandle)
{
    using namespace LibFred::RegistrableObject::Keyset;
    BOOST_CHECK_EXCEPTION(GetKeysetStateHistoryByHandle{nonexistent_handle}.exec(ctx, unlimited),
                          GetKeysetStateHistoryByHandle::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(keyset_id_does_not_exist, HasNonexistentId)
{
    using namespace LibFred::RegistrableObject::Keyset;
    BOOST_CHECK_EXCEPTION(GetKeysetStateHistoryById{nonexistent_id}.exec(ctx, unlimited),
                          GetKeysetStateHistoryById::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(keyset_uuid_does_not_exist, HasNonexistentUuid)
{
    using namespace LibFred::RegistrableObject::Keyset;
    const auto uuid = Util::make_strong<KeysetUuid>(nonexistent_uuid);
    BOOST_CHECK_EXCEPTION(GetKeysetStateHistoryByUuid{uuid}.exec(ctx, unlimited),
                          GetKeysetStateHistoryByUuid::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(nsset_handle_does_not_exist, HasNonexistentHandle)
{
    using namespace LibFred::RegistrableObject::Nsset;
    BOOST_CHECK_EXCEPTION(GetNssetStateHistoryByHandle{nonexistent_handle}.exec(ctx, unlimited),
                          GetNssetStateHistoryByHandle::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(nsset_id_does_not_exist, HasNonexistentId)
{
    using namespace LibFred::RegistrableObject::Nsset;
    BOOST_CHECK_EXCEPTION(GetNssetStateHistoryById{nonexistent_id}.exec(ctx, unlimited),
                          GetNssetStateHistoryById::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(nsset_uuid_does_not_exist, HasNonexistentUuid)
{
    using namespace LibFred::RegistrableObject::Nsset;
    const auto uuid = Util::make_strong<NssetUuid>(nonexistent_uuid);
    BOOST_CHECK_EXCEPTION(GetNssetStateHistoryByUuid{uuid}.exec(ctx, unlimited),
                          GetNssetStateHistoryByUuid::DoesNotExist,
                          [](auto&&) { return true;});
}

BOOST_FIXTURE_TEST_CASE(has_history, HasAllObjectsWithStateHistory)
{
    using namespace LibFred::RegistrableObject;
    LibFred::OperationContextCreator ctx;
    const auto contact_state_history = Contact::GetContactStateHistoryByHandle{contact.data.handle}
            .exec(ctx, unlimited);
    {
        const auto contact_state_history_by_id = Contact::GetContactStateHistoryById{contact.data.id}
                .exec(ctx, unlimited);
        const auto contact_state_history_by_uuid = Contact::GetContactStateHistoryByUuid{contact.data.uuid}
                .exec(ctx, unlimited);
        BOOST_CHECK(contact_state_history == contact_state_history_by_id);
        BOOST_CHECK(contact_state_history == contact_state_history_by_uuid);
    }
    BOOST_CHECK_EQUAL(contact_state_history.timeline.size(), 6);
    BOOST_CHECK_EQUAL(contact_state_history.timeline[0].state, (Contact::ContactState{}.set<Contact::Linked>()));
    BOOST_CHECK_EQUAL(contact_state_history.timeline[1].state, (Contact::ContactState{}.set<Contact::Linked>()));
    BOOST_CHECK_EQUAL(contact_state_history.timeline[2].state, (Contact::ContactState{}.set<Contact::Linked, Contact::IdentifiedContact>()));
    BOOST_CHECK_EQUAL(contact_state_history.timeline[3].state, (Contact::ContactState{}.set<Contact::Linked>()));
    BOOST_CHECK_EQUAL(contact_state_history.timeline[4].state, (Contact::ContactState{}.set<Contact::Linked, Contact::IdentifiedContact>()));
    BOOST_CHECK_EQUAL(contact_state_history.timeline[5].state, (Contact::ContactState{}.set<Contact::Linked, Contact::IdentifiedContact>()));

    const auto domain_state_history = Domain::GetDomainStateHistoryByFqdn{domain.data.fqdn}
            .exec(ctx, unlimited);
    {
        const auto domain_state_history_by_id = Domain::GetDomainStateHistoryById{domain.data.id}
                .exec(ctx, unlimited);
        const auto domain_state_history_by_uuid = Domain::GetDomainStateHistoryByUuid{domain.data.uuid}
                .exec(ctx, unlimited);
        BOOST_CHECK(domain_state_history == domain_state_history_by_id);
        BOOST_CHECK(domain_state_history == domain_state_history_by_uuid);
    }
    BOOST_CHECK_EQUAL(domain_state_history.timeline.size(), 6);
    BOOST_CHECK_EQUAL(domain_state_history.timeline[0].state, (Domain::DomainState{}.set<Domain::NssetMissing, Domain::Outzone>()));
    BOOST_CHECK_EQUAL(domain_state_history.timeline[1].state, (Domain::DomainState{}.set<Domain::NssetMissing, Domain::Outzone>()));
    BOOST_CHECK_EQUAL(domain_state_history.timeline[2].state, (Domain::DomainState{}.set<Domain::NssetMissing, Domain::Outzone, Domain::ServerInzoneManual>()));
    BOOST_CHECK_EQUAL(domain_state_history.timeline[3].state, (Domain::DomainState{}.set<Domain::NssetMissing, Domain::Outzone>()));
    BOOST_CHECK_EQUAL(domain_state_history.timeline[4].state, (Domain::DomainState{}.set<Domain::NssetMissing, Domain::Outzone, Domain::ServerInzoneManual>()));
    BOOST_CHECK_EQUAL(domain_state_history.timeline[5].state, (Domain::DomainState{}.set<Domain::NssetMissing, Domain::Outzone, Domain::ServerInzoneManual>()));

    const auto keyset_state_history = Keyset::GetKeysetStateHistoryByHandle{keyset.data.handle}
            .exec(ctx, unlimited);
    {
        const auto keyset_state_history_by_id = Keyset::GetKeysetStateHistoryById{keyset.data.id}
                .exec(ctx, unlimited);
        const auto keyset_state_history_by_uuid = Keyset::GetKeysetStateHistoryByUuid{keyset.data.uuid}
                .exec(ctx, unlimited);
        BOOST_CHECK(keyset_state_history == keyset_state_history_by_id);
        BOOST_CHECK(keyset_state_history == keyset_state_history_by_uuid);
    }
    BOOST_CHECK_EQUAL(keyset_state_history.timeline.size(), 6);
    BOOST_CHECK_EQUAL(keyset_state_history.timeline[0].state, Keyset::KeysetState{});
    BOOST_CHECK_EQUAL(keyset_state_history.timeline[1].state, Keyset::KeysetState{});
    BOOST_CHECK_EQUAL(keyset_state_history.timeline[2].state, Keyset::KeysetState{}.set<Keyset::ServerDeleteProhibited>());
    BOOST_CHECK_EQUAL(keyset_state_history.timeline[3].state, Keyset::KeysetState{});
    BOOST_CHECK_EQUAL(keyset_state_history.timeline[4].state, Keyset::KeysetState{}.set<Keyset::ServerDeleteProhibited>());
    BOOST_CHECK_EQUAL(keyset_state_history.timeline[5].state, Keyset::KeysetState{}.set<Keyset::ServerDeleteProhibited>());

    const auto nsset_state_history = Nsset::GetNssetStateHistoryByHandle{nsset.data.handle}
            .exec(ctx, unlimited);
    {
        const auto nsset_state_history_by_id = Nsset::GetNssetStateHistoryById{nsset.data.id}
                .exec(ctx, unlimited);
        const auto nsset_state_history_by_uuid = Nsset::GetNssetStateHistoryByUuid{nsset.data.uuid}
                .exec(ctx, unlimited);
        BOOST_CHECK(nsset_state_history == nsset_state_history_by_id);
        BOOST_CHECK(nsset_state_history == nsset_state_history_by_uuid);
    }
    BOOST_CHECK_EQUAL(nsset_state_history.timeline.size(), 6);
    BOOST_CHECK_EQUAL(nsset_state_history.timeline[0].state, Nsset::NssetState{});
    BOOST_CHECK_EQUAL(nsset_state_history.timeline[1].state, Nsset::NssetState{});
    BOOST_CHECK_EQUAL(nsset_state_history.timeline[2].state, Nsset::NssetState{}.set<Nsset::ServerTransferProhibited>());
    BOOST_CHECK_EQUAL(nsset_state_history.timeline[3].state, Nsset::NssetState{});
    BOOST_CHECK_EQUAL(nsset_state_history.timeline[4].state, Nsset::NssetState{}.set<Nsset::ServerTransferProhibited>());
    BOOST_CHECK_EQUAL(nsset_state_history.timeline[5].state, Nsset::NssetState{}.set<Nsset::ServerTransferProhibited>());
}

BOOST_AUTO_TEST_SUITE_END()//TestStateHistory
